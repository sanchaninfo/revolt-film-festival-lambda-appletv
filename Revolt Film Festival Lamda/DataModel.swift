/***
 **Module Name: DataMode
 **File Name :   DataModel.swift
 **Project :   calendow.groundwurk.com
 **Project :   Revolt
 **Copyright(c) : Revolt.
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : MVC Pattern.
 */

import Foundation


class Carousal
{
    var carousel_name:AnyObject!
    var mobile:String!
    var order:String!
    var site:String!
    var status:String!
    var tv:String!
    
    func initWithName(name:AnyObject,mobile:String,order:String,site:String,status:String,tv:String)-> Carousal
    {
        self.carousel_name = name
        self.mobile = mobile
        self.order = order
        self.site = site
        self.status = status
        self.tv = tv
        return self
    }
}

class GetCarousalData
{
    var id:String!,name:String!,description:String!,created_by:String!,updated_by:String!,url:String!,thumb:String!,file_duration:String!,type:String!
    var content_fieldset_id:String!
    var publish:String!
    var metathumb:String!
    var media_type:String!
    var featured:String!
    var genre:String!
    var release_date:String!
    var carousel_id:String!
    var movie_art:String!
    var director:String!
    var tv_show_name:String!
    var season_number:String!
    var episode_number:String!
    var image:String!
    var title:String!
    var cast:String!
    var maturity_rating:String!
    var starring:String!
    var title_image:String!
    var main_carousel:String!
    var main_carousel_image:String!
    var trailer_url:String!
    var img_url:String!
    var title_img_url:String!
    var main_carousel_image_url:String!
    var url_m3u8:String!
    var url_mpd:String!
    var contains:NSArray!
    var tv_show:Bool!
    func initWithName(id:String!,name:String!,description:String!,url:String!,thumb:String!,file_duration:String!,type:String!,publish:String!,metathumb:String!,release_date:String!,carousel_id:String!,movie_art:String!,tv_show_name:String!,season_number:String!,episode_number:String!,image:String!,title:String!,cast:String!,main_carousel_image_url:String!,url_m3u8:String!,url_mpd:String!,contains:NSArray!,tv_show:Bool!
        )-> GetCarousalData
    {
        self.id = id
        self.name = name
        self.description = description
        self.url = url
        self.thumb = thumb
        self.file_duration = file_duration
        self.type = type
        self.publish = publish
        self.metathumb = metathumb
        self.release_date = release_date
        self.carousel_id = carousel_id
        self.movie_art = movie_art
        self.tv_show_name = tv_show_name
        self.season_number = season_number
        self.episode_number = episode_number
        self.image = image
        self.title = title
        self.cast = cast
        self.main_carousel_image_url = main_carousel_image_url
        self.url_m3u8 = url_m3u8
        self.url_mpd = url_mpd
        self.contains = contains
        self.tv_show = tv_show
        return self
    }
}

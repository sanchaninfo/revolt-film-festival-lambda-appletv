/***
 **Module Name: DetailPageViewController
 **File Name :  DetailPageViewController.swift
 **Project :   Revolt
 **Copyright(c) : Revolt.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Asset Details.
 */

import UIKit
import AVKit
import Cosmos


protocol myListdelegate: class
{
    func myListdata(mylistDict:NSDictionary)
    func removeListdata(id: String)
    func recentlyWatcheddata(recentlyWatchedDict:NSDictionary)
}
class DetailPageViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,playerdelegate,epidelegate {
    
    @objc var EpisodesPath = [[String:Any]]()
    @objc var TvshowPath = NSDictionary()
    @objc var userid = String()
    @objc var deviceId,uuid: String!
    @objc var Menus = [String]()
    @objc var isMain = Bool()
    @objc var isRecentlywatch = Bool()
    @objc var iswatchedVideo = Float64()
    @objc var seektime = Float64()
    @objc var tvshowCount = Int()
    @objc var seektimevideo = Float64()
    @objc var subscription_status = String()
    @objc var statusDict = NSDictionary()
    @objc var isMyList = Bool()
    @objc var VideoID = String()
    @objc var dataUrl = String()
    @objc var isUpdated = Bool()
    @objc var fromSearch = Bool()
    @objc var tvshow = Bool()
    @objc var storeProdData = [[String:Any]]()
    @objc var Donatedict = [[String:Any]]()
    weak var delegate:myListdelegate?
    @objc var carouselName = String()
    @objc var fromMyList = Bool()
    @objc var rating:Float = 0.0
    @objc var userrating:Int = 0
    @objc var previousrate: Int = 0
    @objc var userRateValue:Int  = 0
    @objc var nextindex = IndexPath()
    @objc var ratingCell = UITableViewCell()
    @objc var isstarresponse = String()
    


    
    @objc public var numberOfStars: Int = 5
    @objc public var starsSpacing: CGFloat = 30
    @objc public var starFontSize: CGFloat = 100
    @objc public var cornerRadius: CGFloat = 10
    
    @objc public var selectedStarText = "★"
    @objc public var notSelectedStarText = "☆"
    @objc public var selectedColor = UIColor.black
    @objc public var notSelectedColor = UIColor.white
    @objc public var rate: Int = 0 {
        didSet {
            rate = min(numberOfStars, rate)
            rate = max(0, rate)
        }
    }
    @objc public let titleLabel = UILabel()
    @objc public let subtitleLabel = UILabel()
    
    private lazy var buttons: [UIButton] = {
        return (0..<self.numberOfStars).flatMap {_ in
            let button = UIButton(type: .custom)
            button.contentEdgeInsets = UIEdgeInsets.zero
            button.titleLabel?.font = UIFont.systemFont(ofSize: self.starFontSize)
            button.layer.cornerRadius = self.cornerRadius
            button.layer.masksToBounds = true
            return button
        }
    }()
    
    override public var preferredFocusedView: UIView? {
        return buttons[max(0, rate-1)]
    }

    
  
    @IBOutlet weak var cosmosview: CosmosView!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var BackgrndImg: UIImageView!
    @IBOutlet weak var TvLbl: UILabel!
    @IBOutlet weak var TimeLbl: UILabel!
    @IBOutlet weak var ReleaseLbl: UILabel!
    @IBOutlet weak var descriptiontxt: UITextView!
    @IBOutlet var operationsListView:UITableView!
    @IBOutlet var starView: UIView!
    @objc var viewToFocus: UIView? = nil
    {
        didSet
        {
            if viewToFocus != nil
            {
                self.setNeedsFocusUpdate();
                self.updateFocusIfNeeded();
            }
        }
    }
//    override weak var preferredFocusedView: UIView?
//    {
//        if viewToFocus != nil
//        {
//            return viewToFocus;
//        }
//        else
//        {
//            return super.preferredFocusedView
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isstarresponse = "true"
        cosmosview.settings.fillMode = .precise
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getStarrating()
        if (TvshowPath["contains"] != nil)
        {
            if ((TvshowPath["contains"] is NSArray))
            {
                if (TvshowPath["contains"] as! NSArray).count != 0
                {
                    tvshowCount = getTvShowCount(isTvShow: true)
                    tvshow = true
                }
                else
                {
                    tvshowCount = getTvShowCount(isTvShow: false)
                    tvshow = false
                }
            }
            else
            {
                if ((TvshowPath["contains"] as! NSDictionary).count != 0)
                {
                    tvshowCount = getTvShowCount(isTvShow: true)
                    tvshow = true
                }
                else
                {
                    tvshowCount = getTvShowCount(isTvShow: false)
                    tvshow = false
                }
                
                }
           }
        else
        {
            tvshowCount = getTvShowCount(isTvShow: false)
            tvshow = false
        }
        BackgrndImg.kf.indicatorType = .activity
    
        var metaDict = NSDictionary()
        if TvshowPath.object(forKey: "metadata") != nil
        {
            metaDict = TvshowPath.object(forKey: "metadata") as! NSDictionary
            BackgrndImg.kf.setImage(with: URL(string: isnil(json: metaDict, key: "banner_image_1300x650")))
            let str1 = (isnil(json: metaDict, key: "releaseDate")).components(separatedBy: "-")
            ReleaseLbl.text = str1[0]
            self.carouselName = (metaDict["carousel_Name"]  as! String)
            
            
        }
        else
        {
            BackgrndImg.kf.setImage(with: URL(string: isnil(json: TvshowPath, key: "banner_image_1300x650")))
            let str1 = (isnil(json: TvshowPath, key: "releaseDate")).components(separatedBy: "-")
            ReleaseLbl.text = str1[0]

        }
        TvLbl.text = (isnil(json:TvshowPath,key:"assetName")).capitalized
        let labelText = TvLbl.text?.capitalized
        TvLbl?.frame = CGRect(x: (TvLbl?.frame.origin.x)!, y: (TvLbl?.frame.origin.y)!, width: (labelText?.widthWithConstrainedWidth(height: 60, font: (TvLbl?.font)!))!, height: (TvLbl?.frame.size.height)!)
     //   TimeLbl.text =  stringFromTimeInterval(interval: Double(isnil(json:TvshowPath,key:"duration"))!)
        let time = isnil(json:TvshowPath,key:"duration")
        let time1 = Double(time)
        if time1 != nil
        {
            TimeLbl.text =  stringFromTimeInterval(interval: time1!)
        }
        else
        {
            TimeLbl.text = "0 M"
        }
       // TimeLbl.text = "0"
        descriptiontxt.text = isnil(json: TvshowPath, key: "description")
        let DescriptionText = TvshowPath["description"] as? String
        let destxt = DescriptionText?.replacingOccurrences(of: "&", with: "", options: .literal, range: nil)
        let destxt1 =  destxt?.replacingOccurrences(of: "amp", with: "", options: .literal, range: nil)
        let destxt2 = destxt1?.replacingOccurrences(of: ";", with: "", options: .literal, range: nil)
        descriptiontxt.text = destxt2
        descriptiontxt?.frame = CGRect(x: (descriptiontxt?.frame.origin.x)!, y: (descriptiontxt?.frame.origin.y)!, width: (descriptiontxt?.frame.size.width)!, height: (DescriptionText?.widthWithConstrainedWidth(height: 60, font: (descriptiontxt?.font)!))!)
//        DispatchQueue.main.async
//            {
//                UIView.transition(with: self.operationsListView, duration: 1.0, options: .transitionCrossDissolve, animations: {self.operationsListView.reloadData()}, completion: nil)
//        }
        self.operationsListView.reloadData()
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
    
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return tvshowCount
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
            (cell.viewWithTag(11) as! UILabel).textColor = UIColor.white
            cell.viewWithTag(9)!.isHidden = true
        
        
            cell.layer.cornerRadius = 7.0
        var dur  = Int()
        if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] is NSNull
        {
            dur = 0
        }
        else
        {
            if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] is Int
            {
                dur = ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] as! Int
            }
            else
            {
                if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] is String
                {
                    let floatval = Float64(((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] as! String)!
                    dur = Int(floatval)
                    
                }
            }
        }
            
            //Seektime > 0
            if  Double(dur) > 0.0
            {
                switch indexPath.row
                {
                case 0:
                    cell.isUserInteractionEnabled = true
                     (cell.viewWithTag(11) as! UILabel).text = Menus[0]
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                    break
                case 1:
                    cell.isUserInteractionEnabled = true
                     (cell.viewWithTag(11) as! UILabel).text = Menus[1]
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                    break
                case 2:
                    cell.isUserInteractionEnabled = true
                     (cell.viewWithTag(11) as! UILabel).text = Menus[2]
                    if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["myList"] as! Bool == false
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList")
                    }
                    break
                case 3:
                    cell.isUserInteractionEnabled = true
                     (cell.viewWithTag(11) as! UILabel).text = Menus[3]
                    if tvshow == true
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                    }
                    else
                    {
                          (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_rate")
                    }
                    break
                case 4:
                     (cell.viewWithTag(11) as! UILabel).text = Menus[4]
                    cell.isUserInteractionEnabled = true

                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_rate")
                        
                     //   (cell.viewWithTag(11) as! UILabel).text = "Rate this title"
                 
                    break
                    
                default:
                    break
                }
            }
                //  Seektime < 0
            else
            {
                switch indexPath.row
                {
                case 0:
                    cell.isUserInteractionEnabled = true
                    (cell.viewWithTag(11) as! UILabel).text = Menus[0]
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                    break
                case 1:
                    cell.isUserInteractionEnabled = true
                     (cell.viewWithTag(11) as! UILabel).text = Menus[1]
                    if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["myList"] as! Bool == false
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList")
                    }
                    break
                case 2:
                    cell.isUserInteractionEnabled = true
                     (cell.viewWithTag(11) as! UILabel).text = Menus[2]
                    if tvshow == true
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_rate")
                    }
                    break
                case 3:
                     (cell.viewWithTag(11) as! UILabel).text = Menus[3]
                    cell.isUserInteractionEnabled = true

                      (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_rate")
                       // (cell.viewWithTag(11) as! UILabel).text = "Rate this title"
                     break
                default:
                    break
                }
            }
            return cell
        
    }
    
    func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        // previous focus view
        
        if let previousIndexPath = context.previouslyFocusedIndexPath,
            let cell = tableView.cellForRow(at: previousIndexPath)
            
        {
            cell.viewWithTag(9)!.isHidden = true
            (cell.viewWithTag(11) as! UILabel).textColor = UIColor.white
            var dur  = Int()
            if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] is NSNull
            {
                dur = 0
            }
            else
            {
                if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] is Int
                {
                    dur = ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] as! Int
                }
                else
                {
                    if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] is String
                    {
                        let floatval = Float64(((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] as! String)!
                        dur = Int(floatval)
                       
                    }
                }
            }
           
                if  Double(dur) > 0.0
            {
                switch previousIndexPath.row
                {
                case 0:
                    
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                    break
                case 1:
                  
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                    break
                case 2:
                    
                    if (cell.viewWithTag(11) as! UILabel).text == "Add To MyList"
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList")
                    }
                    break
                case 3:
                    if tvshow == true
                    {
                        
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                    }
                    else
                    {
                        // (cell.viewWithTag(11) as! UILabel).textColor = UIColor.white
                        (cell.viewWithTag(10) as! UIImageView).isHidden = false
                        (cell.viewWithTag(11) as! UILabel).isHidden = false
                        
                        (cell.viewWithTag(9)!).isHidden = true
                        
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_rate")
                        
                        (cell.viewWithTag(11) as! UILabel).text = "Rate this title"
                    }
                    break
                case 4:
                        // (cell.viewWithTag(11) as! UILabel).textColor = UIColor.white
                        (cell.viewWithTag(10) as! UIImageView).isHidden = false
                        (cell.viewWithTag(11) as! UILabel).isHidden = false
                    
                        (cell.viewWithTag(9)!).isHidden = true
                    
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_rate")

                       (cell.viewWithTag(11) as! UILabel).text = "Rate this title"
                    
                    
                    break
            
    
                default:
                    break
                }
            }
            else
            {
                switch previousIndexPath.row
                {
                case 0:
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1")
                    break
                case 1:
                    
                    if (cell.viewWithTag(11) as! UILabel).text == "Add To MyList"
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList")
                    }
                    break
                case 2:
                    if tvshow == true
                    {
                        
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                    }
                    else
                    {
                         (cell.viewWithTag(11) as! UILabel).textColor = UIColor.white
                        (cell.viewWithTag(10) as! UIImageView).isHidden = false
                        (cell.viewWithTag(11) as! UILabel).isHidden = false
                         (cell.viewWithTag(9)!).isHidden = true
                    
                        
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_rate")
                        
                        (cell.viewWithTag(11) as! UILabel).text = "Rate this title"

                    }
                    break
                case 3:
                    
                    (cell.viewWithTag(10) as! UIImageView).isHidden = false
                    (cell.viewWithTag(11) as! UILabel).isHidden = false
                     (cell.viewWithTag(9)!).isHidden = true
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_rate")
//                    
                    (cell.viewWithTag(11) as! UILabel).text = "Rate this title"
                default:
                    break
                }
            }
        }
        // next focus view
        if let nextIndexPath = context.nextFocusedIndexPath,
            let cell = tableView.cellForRow(at: nextIndexPath)
        {
            
               ratingCell = cell
            var dur  = Int()
            if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] is NSNull
            {
                dur = 0
            }
            else
            {
                if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] is Int
                {
                    dur = ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] as! Int
                }
                else
                {
                    if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] is String
                    {
                        let floatval = Float64(((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] as! String)!
                        dur = Int(floatval)
                      
                        
                    }
                }
            }
                if  Double(dur) > 0.0
            {
                switch nextIndexPath.row
                {
                case 0:
                     (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1_hover")
                    break
                case 1:
                     (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1_hover")
                    break
                case 2:
                     (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
                    if (cell.viewWithTag(11) as! UILabel).text == "Add To MyList"
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist_hover")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList_hover")
                    }
                    break
                case 3:
                    if tvshow == true
                    {
                         (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                    }
                    else
                    {
                         (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
                      
                        
                        (cell.viewWithTag(10) as! UIImageView).isHidden = true
                        (cell.viewWithTag(11) as! UILabel).isHidden = true
                     //   (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "")
                        (cell.viewWithTag(11) as! UILabel).text = ""
                     //   cell.bringSubview(toFront: cell.viewWithTag(9)!)
                         self.userRateValue = self.userrating
                        cell.viewWithTag(9)!.isHidden = false
                        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
                        swipeRight.direction = UISwipeGestureRecognizerDirection.right
                        cell.viewWithTag(9)!.addGestureRecognizer(swipeRight)
                        
                        
                        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
                        swipeDown.direction = UISwipeGestureRecognizerDirection.left
                        cell.viewWithTag(9)!.addGestureRecognizer(swipeDown)
                        getrateUpdate()
                    }
                    break
                case 4:
                     (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
        
                    (cell.viewWithTag(10) as! UIImageView).isHidden = true
                    (cell.viewWithTag(11) as! UILabel).isHidden = true
                   //   cell.bringSubview(toFront: cell.viewWithTag(9)!)
                    // (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "")
                     (cell.viewWithTag(11) as! UILabel).text = ""
                      self.userRateValue = self.userrating
                    cell.viewWithTag(9)!.isHidden = false
                    let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
                    swipeRight.direction = UISwipeGestureRecognizerDirection.right
                    cell.viewWithTag(9)!.addGestureRecognizer(swipeRight)
                    
                    
                    let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
                    swipeDown.direction = UISwipeGestureRecognizerDirection.left
                    cell.viewWithTag(9)!.addGestureRecognizer(swipeDown)
                    getrateUpdate()
                    break
    
                default:
                    break
                }
            }
            else
            {
                switch nextIndexPath.row
                {
                case 0:
                     (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
                    (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "ps1_hover")
                    break
                case 1:
                     (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
                    if (cell.viewWithTag(11) as! UILabel).text == "Add To MyList"
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist_hover")
                    }
                    else
                    {
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList_hover")
                    }
                    break
                case 2:
                    if tvshow == true
                    {
                         (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
                        (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "episodes")
                    }
                    else
                    {
                         (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
              
                        (cell.viewWithTag(10) as! UIImageView).isHidden = true
                        (cell.viewWithTag(11) as! UILabel).isHidden = true
                    //     cell.bringSubview(toFront: cell.viewWithTag(9)!)
                      //  (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "")
                        (cell.viewWithTag(11) as! UILabel).text = ""
                        cell.viewWithTag(9)!.isHidden = false
                         self.userRateValue = self.userrating
                        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
                        swipeRight.direction = UISwipeGestureRecognizerDirection.right
                        cell.viewWithTag(9)!.addGestureRecognizer(swipeRight)
                        //  self.view.addGestureRecognizer(swipeRight)
                        
                        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
                        swipeDown.direction = UISwipeGestureRecognizerDirection.left
                        cell.viewWithTag(9)!.addGestureRecognizer(swipeDown)
                        
                        
                        getrateUpdate()
                        
                    }
                    break
                case 3:
                     (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
         
                    (cell.viewWithTag(10) as! UIImageView).isHidden = true
                    (cell.viewWithTag(11) as! UILabel).isHidden = true
                    // (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "")
                     (cell.viewWithTag(11) as! UILabel).text = ""
                  //    cell.bringSubview(toFront: cell.viewWithTag(9)!)
                    cell.viewWithTag(9)!.isHidden = false
                    let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
                    swipeRight.direction = UISwipeGestureRecognizerDirection.right
                    cell.viewWithTag(9)!.addGestureRecognizer(swipeRight)
                    //  self.view.addGestureRecognizer(swipeRight)
                    self.userRateValue = self.userrating
                    let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
                    swipeDown.direction = UISwipeGestureRecognizerDirection.left
                    cell.viewWithTag(9)!.addGestureRecognizer(swipeDown)
                    
                    
                    getrateUpdate()
                    break
                default:
                    break
                }
            }
        }
        
        
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
               if self.userRateValue >= 0 && self.userRateValue < 6
               {
              //  self.userrating = self.userrating + 1
                self.userRateValue = self.userRateValue + 1
                getrateUpdate()
               }
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
               if self.userRateValue > 0  && self.userRateValue <= 6
               {
              //  self.userrating = self.userrating - 1
                self.userRateValue = self.userRateValue - 1
                getrateUpdate()
              }
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    @objc func getrateUpdate()
    {
        switch  self.userRateValue {
        case 0:
            (ratingCell.viewWithTag(9)!.viewWithTag(1) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(2) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(3) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(4) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(5) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(6) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
        
            
            break
        case 1:
    
            
            (ratingCell.viewWithTag(9)!.viewWithTag(1) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
            (ratingCell.viewWithTag(9)!.viewWithTag(2) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(3) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(4) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(5) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(6) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            
            
            break
        case 2:
            
            (ratingCell.viewWithTag(9)!.viewWithTag(1) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
            (ratingCell.viewWithTag(9)!.viewWithTag(2) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
            (ratingCell.viewWithTag(9)!.viewWithTag(3) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(4) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(5) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(6) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")

            
            break
        case 3:
            (ratingCell.viewWithTag(9)!.viewWithTag(1) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
            (ratingCell.viewWithTag(9)!.viewWithTag(2) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
            (ratingCell.viewWithTag(9)!.viewWithTag(3) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
            (ratingCell.viewWithTag(9)!.viewWithTag(4) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(5) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(6) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            
            break
        case 4:
            (ratingCell.viewWithTag(9)!.viewWithTag(1) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
            (ratingCell.viewWithTag(9)!.viewWithTag(2) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
            (ratingCell.viewWithTag(9)!.viewWithTag(3) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
            (ratingCell.viewWithTag(9)!.viewWithTag(4) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
            (ratingCell.viewWithTag(9)!.viewWithTag(5) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(6) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            
            break
        case 5:
            (ratingCell.viewWithTag(9)!.viewWithTag(1) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
            (ratingCell.viewWithTag(9)!.viewWithTag(2) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
            (ratingCell.viewWithTag(9)!.viewWithTag(3) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
            (ratingCell.viewWithTag(9)!.viewWithTag(4) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
            (ratingCell.viewWithTag(9)!.viewWithTag(5) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
            (ratingCell.viewWithTag(9)!.viewWithTag(6) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            
            break
        case 6:
        (ratingCell.viewWithTag(9)!.viewWithTag(1) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
        (ratingCell.viewWithTag(9)!.viewWithTag(2) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
        (ratingCell.viewWithTag(9)!.viewWithTag(3) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
        (ratingCell.viewWithTag(9)!.viewWithTag(4) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
        (ratingCell.viewWithTag(9)!.viewWithTag(5) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
        (ratingCell.viewWithTag(9)!.viewWithTag(6) as! UIImageView).image = UIImage(imageLiteralResourceName: "star")
        
            break
        default:
            (ratingCell.viewWithTag(9)!.viewWithTag(1) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(2) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(3) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(4) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(5) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            (ratingCell.viewWithTag(9)!.viewWithTag(6) as! UIImageView).image = UIImage(imageLiteralResourceName: "star_white")
            break
        }
    }
    
   /* func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        let cell = tableView.cellForRow(at: indexPath)
        print(indexPath)
//        if (cell?.viewWithTag(11) as! UILabel).text == "Rate this title"
//        {
//            return false
//        }
//        else
//        {
//            return true
//        }
        return false
    }*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
      
       
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let PlayerVC = storyBoard.instantiateViewController(withIdentifier: "playerLayer") as! PlayerLayerViewController
        var monotize = String()
        var monotizeLabel = String()
        var montizetype = String()
      //  var montizeURL = String()
        var tvauthRequired = String()
        let subscriptionPage = storyBoard.instantiateViewController(withIdentifier: "subscription") as! SubscriptionViewController
         
        if TvshowPath.object(forKey: "metadata") != nil
        {
            
            let metaDict = TvshowPath.object(forKey: "metadata") as! NSDictionary
            if metaDict["monetize"] != nil && metaDict["monetizeLabel"] != nil && metaDict["monetizeType"] != nil
            {
                if metaDict["monetize"] is String
                {
                    monotize = metaDict["monetize"] as! String
                }
                else
                {
                    monotize = ""
                }
                if metaDict["monetizeLabel"] is String
                {
                    monotizeLabel = metaDict["monetizeLabel"] as! String
                }
                else
                {
                    monotizeLabel = ""
                }
                if metaDict["monetizeType"] is String
                {
                    montizetype = metaDict["monetizeType"] as! String
                }
                else
                {
                    montizetype = ""
                }
            }
            if ((metaDict["tve_provider_authorization"] != nil))
            {
                    if metaDict["tve_provider_authorization"] is NSString
                    {
                        if metaDict["tve_provider_authorization"] as! String == "true"
                        {
                            if TvshowPath["provider"] != nil
                            {
                                if (TvshowPath["provider"] as! String == "") || (TvshowPath["provider"] as! String == " ")
                                {
                                    tvauthRequired = "true"
                                }
                                else
                                {
                                    tvauthRequired = "false"
                                }
                            }
                            else
                            {
                                tvauthRequired = "true"
                            }
                           
                        }
                        else
                        {
                           tvauthRequired = "false"
                        }
                    }
                    if metaDict["tve_provider_authorization"] is Bool
                    {
                        if metaDict["tve_provider_authorization"] as! Bool == true
                        {
                            if TvshowPath["provider"] != nil
                            {
                                if (TvshowPath["provider"] as! String == "") || (TvshowPath["provider"] as! String == " ")
                                {
                                    tvauthRequired = "true"
                                }
                                else
                                {
                                    tvauthRequired = "false"
                                }
                            }
                            else
                            {
                                tvauthRequired = "true"
                            }
                        }
                        else
                        {
                            tvauthRequired = "false"
                        }
                    }
                
//                if (metaDict["tve_provider_authorization"] is Bool)
//                {
//                    if (metaDict["tve_provider_authorization"] as! Bool == true)
//                    {
//                        tvauthRequired = "true"
//                    }
//                    else
//                    {
//                        tvauthRequired = "false"
//                    }
//                }
            }
            else
            {
                tvauthRequired = "false"
            }
        }
            //monetizeLabel
        else
        {
            if TvshowPath["monetize"] != nil && TvshowPath["monetizeLabel"] != nil && TvshowPath["monetizeType"] != nil
            {
            if TvshowPath["monetize"] is String
            {
                 monotize = TvshowPath["monetize"] as! String
            }
            else
            {
                monotize = ""
            }
                if TvshowPath["monetizeLabel"] is String
                {
                    monotizeLabel = TvshowPath["monetizeLabel"] as! String
                }
                else
                {
                    monotizeLabel = ""
                }
                if TvshowPath["monetizeType"] is String
                {
                    montizetype = TvshowPath["monetizeType"] as! String
                }
                else
                {
                    montizetype = ""
                }
           
            }
            if TvshowPath["tve_provider_authorization"] != nil
            {
                if (TvshowPath["tve_provider_authorization"] is NSString)
                {
                    if (TvshowPath["tve_provider_authorization"] as! String == "true")
                    {
                        tvauthRequired = "true"
                    }
                    else
                    {
                        tvauthRequired = "false"
                    }
                }
                if (TvshowPath["tve_provider_authorization"] is Bool)
                {
                    if (TvshowPath["tve_provider_authorization"] as! Bool == true)
                    {
                        tvauthRequired = "true"
                    }
                    else
                    {
                        tvauthRequired = "false"
                    }
                }
            }
            else
            {
                tvauthRequired = "false"
            }
        }
        var dur  = Int()
        if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] is NSNull
        {
            dur = 0
        }
        else
        {
            if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] is Int
            {
                dur = ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] as! Int
            }
            else
            {
                if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] is String
                {
                    let floatval = Float64(((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] as! String)!
                    dur = Int(floatval)
                 
                    
                }
            }
        }
        if  Double(dur) > 0.0
        {
            switch indexPath.row
            {
            case 0:
//                if (TvshowPath["subscription_status"] as! String) == "active"
//                {
                    PlayerVC.isResume = true
                    if TvshowPath["m3u8_url"] != nil && TvshowPath["m3u8_url"] as! String != ""
                    {
                        PlayerVC.videoUrl = TvshowPath["m3u8_url"] as! String
                    }
                    else
                    {
                        if TvshowPath["url"] != nil && TvshowPath["url"] as! String != ""
                        {
                            PlayerVC.videoUrl = TvshowPath["url"] as! String
                        }
                        else
                        {
                            if TvshowPath["mp4_url"] != nil && TvshowPath["mp4_url"] as! String != ""
                            {
                                PlayerVC.videoUrl = TvshowPath["mp4_url"] as! String
                            }
                        }
                        
                    }
                    PlayerVC.mainVideoID = TvshowPath["assetId"] as! String
                 
                    PlayerVC.resumeTime = Float64(dur)
                    PlayerVC.deviceID = self.deviceId
                    PlayerVC.isMyList = self.fromMyList
                    PlayerVC.userID = self.userid
                    PlayerVC.videoID = (TvshowPath["assetId"] as! String)
                    PlayerVC.uuid = self.uuid
                    PlayerVC.monitizeLbl = monotizeLabel
                    PlayerVC.monitizetype = montizetype
                    PlayerVC.DonateData = self.Donatedict
                    PlayerVC.storeData = self.storeProdData
                    PlayerVC.carouselName = self.carouselName
                    PlayerVC.isfromMylist = self.fromMyList
                    if monotize == "true"
                    {
    
                        if (TvshowPath["monetize_data_source_url"] as! String) == "Products.json"
                        {
                            PlayerVC.isDonate = false
                        }
                        else
                        {
                            PlayerVC.isDonate = true

                        }
                       // PlayerVC.DonateData = self.Donatedict
                        
                    }
                    else
                    {
                      //  PlayerVC.storeData = self.storeProdData
                        PlayerVC.isDonate = false

                    }
                    
            
                   // self.navigationController?.pushViewController(PlayerVC, animated: true)
                    if fromSearch == true
                    {
                        PlayerVC.issearch = true
                        self.present(PlayerVC, animated: true, completion: nil)
                    }
                    else
                    {
                        PlayerVC.issearch = false
                        if (((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["subscription_status"] as! String) == "active"
                        {
                            self.navigationController?.pushViewController(PlayerVC, animated: true)
                        }
                        else
                        {
                            self.navigationController?.pushViewController(subscriptionPage, animated: true)
                        }
                      /*  if tvauthRequired == "true"
                        {
//                            let alertview = UIAlertController(title: "Please Verify your Subscription", message: "To Subscribe go to \(kBaseUrl)" , preferredStyle: .alert)
//                            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                                UIAlertAction in
//                                self.navigationController?.popViewController(animated: true)
//                            })
//                            alertview.addAction(defaultAction)
//                            self.navigationController?.present(alertview, animated: false, completion: nil)
                            
                            let ProviderPage = storyBoard.instantiateViewController(withIdentifier: "Provider") as! ProviderViewController
                            ProviderPage.UserId = self.userid
                            ProviderPage.videoId = TvshowPath["id"] as! String
                            self.navigationController?.pushViewController(ProviderPage, animated: true)

                            
                        }
                        else
                        {
                            self.navigationController?.pushViewController(PlayerVC, animated: true)
                        }*/
                       // self.navigationController?.pushViewController(PlayerVC, animated: true)
                    }
                    
               // }
               // else
               // {
               //     self.navigationController?.pushViewController(subscriptionPage, animated: true)
               // }
                
                break
            case 1:
//                if (TvshowPath["subscription_status"] as! String) == "active"
//                {
                    RecentlyWatched(withurl:kRecentlyWatchedUrl)
                    if TvshowPath["m3u8_url"] != nil && TvshowPath["m3u8_url"] as! String != ""
                    {
                        PlayerVC.videoUrl = TvshowPath["m3u8_url"] as! String
                    }
                    else
                    {
                        if TvshowPath["url"] != nil && TvshowPath["url"] as! String != ""
                        {
                            PlayerVC.videoUrl = TvshowPath["url"] as! String
                        }
                        else
                        {
                            if TvshowPath["mp4_url"] != nil && TvshowPath["mp4_url"] as! String != ""
                            {
                                PlayerVC.videoUrl = TvshowPath["mp4_url"] as! String
                            }
                        }
                    }
                    PlayerVC.mainVideoID = TvshowPath["assetId"] as! String
                    PlayerVC.deviceID = self.deviceId
                    PlayerVC.isMyList = self.isMyList
                    PlayerVC.userID = self.userid
                    PlayerVC.videoID = (TvshowPath["assetId"] as! String)
                    PlayerVC.storeData = self.storeProdData
                    PlayerVC.uuid = self.uuid
                    PlayerVC.DonateData = self.Donatedict
                    PlayerVC.monitizeLbl = monotizeLabel
                    PlayerVC.monitizetype = montizetype
                    PlayerVC.carouselName = self.carouselName
                    PlayerVC.isfromMylist = self.fromMyList

                    if monotize == "true"
                    {
                        if (TvshowPath["monetize_data_source_url"] as! String) == "Products.json"
                        {
                            PlayerVC.isDonate = false
                        }
                        else
                        {
                            PlayerVC.isDonate = true
                            
                        }

                    }
                    else
                    {
                        //PlayerVC.storeData = self.storeProdData
                        PlayerVC.isDonate = false
                    }
                    if fromSearch == true
                    {
                        PlayerVC.issearch = true
                        self.present(PlayerVC, animated: true, completion: nil)
                    }
                    else
                    {
                        PlayerVC.issearch = false
                        if (((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["subscription_status"] as! String) == "active"
                        {
                            self.navigationController?.pushViewController(PlayerVC, animated: true)
                        }
                        else
                        {
                            self.navigationController?.pushViewController(subscriptionPage, animated: true)
                        }
                      /*  if tvauthRequired == "true"
                        {
//                            let alertview = UIAlertController(title: "Please Verify your Subscription", message: "To Subscribe go to \(kBaseUrl) " , preferredStyle: .alert)
//                            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                                UIAlertAction in
//                                self.navigationController?.popViewController(animated: true)
//                            })
//                            alertview.addAction(defaultAction)
//                            self.navigationController?.present(alertview, animated: false, completion: nil)
                            let ProviderPage = storyBoard.instantiateViewController(withIdentifier: "Provider") as! ProviderViewController
                            ProviderPage.UserId = self.userid
                            ProviderPage.videoId = TvshowPath["id"] as! String
                            self.navigationController?.pushViewController(ProviderPage, animated: true)

                        }
                        else
                        {
                           self.navigationController?.pushViewController(PlayerVC, animated: true)
                        }*/
                         // self.navigationController?.pushViewController(PlayerVC, animated: true)

                        
                    }
                    
                    
                    
                    

//                }
//                else
//                {
//                    self.navigationController?.pushViewController(subscriptionPage, animated: true)
//                }
                break
            case 2:
                ManageList(withurl:kManageListUrl,forPath:indexPath,assetid:(TvshowPath["assetId"] as! String))
                break
            case 3:
                if tvshow
                {
                    let StoryBoard = self.storyboard?.instantiateViewController(withIdentifier: "Episodes") as! EpisodesViewController
                    if TvshowPath["contains"] is NSArray
                    {
                        let dict = TvshowPath["contains"] as! NSArray
                        
                        StoryBoard.containsDict = dict
                    }
                    else
                    {
                        StoryBoard.contains = TvshowPath["contains"] as! NSDictionary
                    }
                    StoryBoard.UserID = self.userid
                    StoryBoard.videoId = TvshowPath["assetId"] as! String
                    StoryBoard.navigationController?.isNavigationBarHidden = true
                    StoryBoard.episodeDelegate = delegate
                    StoryBoard.deviceId = self.deviceId
                    StoryBoard.uuid = self.uuid
                    StoryBoard.Donatedict = self.Donatedict
                    
                    StoryBoard.storeProdData = self.storeProdData
                    if fromSearch
                    {
                        StoryBoard.isfromsearch = true
                        self.present(StoryBoard, animated: true, completion: nil)
                    }
                    else
                    {
                        StoryBoard.isfromsearch = false
                       self.navigationController?.pushViewController(StoryBoard, animated: false)
                    }
                    
                   // self.present(StoryBoard, animated: true, completion: nil)
                }
                else
                {
                    ratingCell.isUserInteractionEnabled = false
                    
                    if isstarresponse == "true"
                    {
                      self.isstarresponse = "false"
                      getuserRatingUpdate()
                   //   ratingCell.isUserInteractionEnabled = true
                        
                    }
                    
                }
                break
            case 4:
                ratingCell.isUserInteractionEnabled = false
                if isstarresponse == "true"
                {
                    self.isstarresponse = "false"
                    getuserRatingUpdate()
                   // ratingCell.isUserInteractionEnabled = true
                }
               
                break
            default:
                break
            }
        }
        else
        {
            switch indexPath.row
            {
            case 0:
//                if (TvshowPath["subscription_status"] as! String) == "active"
//                {
                    RecentlyWatched(withurl:kRecentlyWatchedUrl)
                    if TvshowPath["m3u8_url"] != nil && TvshowPath["m3u8_url"] as! String != ""
                    {
                        PlayerVC.videoUrl = TvshowPath["m3u8_url"] as! String
                      

                    }
                    else
                    {
                        if TvshowPath["url"] != nil && TvshowPath["url"] as! String != ""
                        {
                            PlayerVC.videoUrl = TvshowPath["url"] as! String
                        }
                        else
                        {
                            if TvshowPath["mp4_url"] != nil && TvshowPath["mp4_url"] as! String != ""
                            {
                                PlayerVC.videoUrl = TvshowPath["mp4_url"] as! String
                            }
                        }
                    }
                   
                    PlayerVC.mainVideoID = TvshowPath["assetId"] as! String
                    PlayerVC.userID = userid
                    PlayerVC.videoID = (TvshowPath["assetId"] as! String)
                    PlayerVC.deviceID = deviceId
                    PlayerVC.isMyList = isMyList
                    PlayerVC.storeData = self.storeProdData
                    PlayerVC.uuid = self.uuid
                    PlayerVC.DonateData = self.Donatedict
                    PlayerVC.monitizeLbl = monotizeLabel
                    PlayerVC.monitizetype = montizetype
                    PlayerVC.carouselName = self.carouselName
                    PlayerVC.isfromMylist = self.fromMyList

                    if monotize == "true"
                    {
                        if (TvshowPath["monetize_data_source_url"] as! String) == "Products.json"
                        {
                            PlayerVC.isDonate = false
                        }
                        else
                        {
                            PlayerVC.isDonate = true
                            
                        }
                    }
                    else
                    {
                     //   PlayerVC.storeData = self.storeProdData
                        PlayerVC.isDonate = false
                    }
                    if fromSearch == true
                    {
                        PlayerVC.issearch = true
                        
                      self.present(PlayerVC, animated: true, completion: nil)
                    }
                    else
                    {
                        PlayerVC.issearch = false
                        if (((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["subscription_status"] as! String) == "active"
                        {
                            self.navigationController?.pushViewController(PlayerVC, animated: true)
                        }
                        else
                        {
                            self.navigationController?.pushViewController(subscriptionPage, animated: true)
                        }
                      /*  if tvauthRequired == "true"
                        {
//                            let alertview = UIAlertController(title: "Please Verify your Subscription", message: "To Subscribe go to \(kBaseUrl)" , preferredStyle: .alert)
//                            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                                UIAlertAction in
//                                self.navigationController?.popViewController(animated: true)
//                            })
//                            alertview.addAction(defaultAction)
//                            self.navigationController?.present(alertview, animated: false, completion: nil)
//                         //   self.navigationController?.pushViewController(alertview, animated: true)
                            let ProviderPage = storyBoard.instantiateViewController(withIdentifier: "Provider") as! ProviderViewController
                            ProviderPage.UserId = self.userid
                            ProviderPage.videoId = TvshowPath["id"] as! String
                            self.navigationController?.pushViewController(ProviderPage, animated: true)

                            
                            
                        }
                        else
                        {
                            self.navigationController?.pushViewController(PlayerVC, animated: true)
                        }*/
                     //   self.navigationController?.pushViewController(PlayerVC, animated: true)
                    }
//              }
//               else
//                {
//                    self.navigationController?.pushViewController(subscriptionPage, animated: true)
//                }
                break
            case 1:
                ManageList(withurl:kManageListUrl,forPath:indexPath,assetid:(TvshowPath["assetId"] as! String))
                break
            case 2:
                if tvshow == true
                {
                    let StoryBoard = self.storyboard?.instantiateViewController(withIdentifier: "Episodes") as! EpisodesViewController
                    if TvshowPath["contains"] is NSArray
                    {
                        let dict = TvshowPath["contains"] as! NSArray
                      
                        StoryBoard.containsDict = dict
                    }
                    else
                    {
                        StoryBoard.contains = TvshowPath["contains"] as! NSDictionary
                    }
                   
                    StoryBoard.UserID = self.userid
                    StoryBoard.videoId = TvshowPath["assetId"] as! String
                    StoryBoard.navigationController?.isNavigationBarHidden = true
                    StoryBoard.episodeDelegate = delegate
                    StoryBoard.deviceId = self.deviceId
                    StoryBoard.uuid = self.uuid
                    StoryBoard.Donatedict = self.Donatedict
                    StoryBoard.storeProdData = self.storeProdData
                    if fromSearch
                    {
                        StoryBoard.isfromsearch = true
                        self.present(StoryBoard, animated: true, completion: nil)
                    }
                    else
                    {
                        StoryBoard.isfromsearch = false
                        self.navigationController?.pushViewController(StoryBoard, animated: false)
                    }
                }
                else
                {
                    ratingCell.isUserInteractionEnabled = false
                    if isstarresponse == "true"
                    {
                        self.isstarresponse = "false"
                        getuserRatingUpdate()
                      //  ratingCell.isUserInteractionEnabled = true
                    }
                }
              
                break
            case 3:
                ratingCell.isUserInteractionEnabled = false
                if isstarresponse == "true"
                {
                    self.isstarresponse = "false"
                    getuserRatingUpdate()
                   // ratingCell.isUserInteractionEnabled = true
                }
               
                break
            default:
                break
            }
        }
        PlayerVC.detdelegate = self
    
    }
//    func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
//        var type = Bool()
//        if  ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] as! Float64 > 0.0
//        {
//            if tvshow == true
//            {
//            
//            }
//        }
//        return type
//    }

//    func tablefocus()
//    {
//        
//    }
    func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
      
       return true
    }
    
    // Finding TVShow or not
    @objc func getTvShowCount(isTvShow:Bool)->Int
    {
        var dur = Int()
        Menus.removeAll()
        if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] is NSNull
        {
            dur = 0
        }
        else
        {
            if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] is Int
            {
                dur = ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] as! Int
            }
            else
            {
                if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] is String
                {
                    let floatval = Float64(((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] as! String)!
                    dur = Int(floatval)
                    
                    
                }
            }
            
        }
        let watchtime = Double(dur)
        if  watchtime > 0.0
        {
            Menus.append("Resume playing")
        }
        if isTvShow
        {
            
            Menus.append("Play")
            if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["myList"] as! Bool == false
            {
                Menus.append("Add To MyList")
            }
            else
            {
                Menus.append("Remove from MyList ")
            }
            Menus.append("More Episodes")
            Menus.append("Rate this title")
        }
        else
        {
            Menus.append("Play")
            if ((TvshowPath["userData"] as! NSArray).firstObject as! NSDictionary)["myList"] as! Bool == false
            {
                Menus.append("Add To MyList")
            }
            else
            {
                Menus.append("Remove from MyList ")
            }
            Menus.append("Rate this title")
            
        }
        
        return Menus.count
    }
    
    
        @objc func selectStar(_ button: UIButton) {
            rate = buttons.index(of: button)! + 1
          //  delegate?.ratingViewControllerDidRate(self, rate: rate)
        }
         override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
            if let button = context.previouslyFocusedView as? UIButton {
                button.backgroundColor = UIColor.clear
            }
            if let button = context.nextFocusedView as? UIButton {
                button.backgroundColor = UIColor.white
                rate = buttons.index(of: button)! + 1
                update()
            }
        }
        
        @objc func update() {
            (0..<min(rate, numberOfStars)).forEach { i in
                buttons[i].setTitle(self.selectedStarText, for: UIControlState())
                buttons[i].setTitleColor(self.selectedColor, for: UIControlState())
            }
            (rate..<numberOfStars).forEach { i in
                buttons[i].setTitle(self.notSelectedStarText, for: UIControlState())
                buttons[i].setTitleColor(self.notSelectedColor, for: UIControlState())
            }
        }
    
    // getStar Rating
    @objc func getStarrating()
    {
       // let parameters = ["getAssetRating":["assetId":TvshowPath["assetId"]! as AnyObject,"userId":gUser_ID as AnyObject]]
      //  let url = "http://34.200.110.244:9000/getAssetRating"
     /*   ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters){
            (responseDict,error,isDone)in
            if error == nil
            {
              let JSON = responseDict
              let arr = JSON as! NSArray
              let dict = arr.firstObject as! NSDictionary
         
                if (dict["averagerating"] is NSNull)
                {
                    self.rating = Float(0.0)
                }
                if (dict["averagerating"] is Int)
                {
                    self.rating = Float(dict["averagerating"] as! Float)
                }
                if dict["userassetrating"] is NSNull
                {
                    self.userrating  = Int(0)
                    self.previousrate = Int(0)
                }
                if dict["userassetrating"] is Int
                {
                    self.userrating = dict["userassetrating"] as! Int
                    self.userRateValue = dict["userassetrating"] as! Int
                    self.previousrate = self.userrating
                }
              self.cosmosview.rating = Double(self.rating)
              self.ratingLbl.text = "(\(self.rating)/\(self.cosmosview.settings.totalStars))"
            //  self.rateLbl.text = "(\(self.rating)/\(self.cosmosview.settings.totalStars))"

            }
            else
            {
                self.cosmosview.rating = Double(self.rating)
                self.ratingLbl.text = "(\(self.rating)/\(self.cosmosview.settings.totalStars))"
               // self.rateLbl.text = "(\(self.rating)/\(self.cosmosview.settings.totalStars))"
                print(error?.localizedDescription)
            }
        }*/
        let url = kgetratingUrl + "appname=\(kAppName)&token=\(self.userid)&assetId=\(TvshowPath["assetId"]!)"
     
        ApiManager.sharedManager.postDataWithJsonLambda(url: url, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                if JSON["statusCode"] != nil
                {
                    let status = JSON["statusCode"] as! Int
                    if status == 200
                    {
                        let result = JSON["result"] as! NSArray
                        let dict = result.firstObject as! NSDictionary
                       
                        
                        if (dict["averagerating"] is NSNull)
                        {
                            self.rating = 0.0
                        }
                        if (dict["averagerating"] is Int)
                        {
                            self.rating = Float(dict["averagerating"] as! Float)
                        }
                        if dict["userassetrating"] is Int
                        {
                            self.userrating = dict["userassetrating"] as! Int
                            self.previousrate = dict["userassetrating"] as! Int
                        }
                        if dict["userassetrating"] is NSNull
                        {
                            self.userrating  = Int(0)
                            self.previousrate = Int(0)
                        }
                        
                        self.cosmosview.rating = Double(self.rating)
                        self.ratingLbl.text = "(\(self.rating)/\(self.cosmosview.settings.totalStars))"
                        
                    }
                    else
                    {
                        self.cosmosview.rating = Double(self.rating)
                        self.ratingLbl.text = "\(self.rating) / \(self.cosmosview.settings.totalStars)"
                    }
                }
                else
                {
                    self.cosmosview.rating = Double(self.rating)
                    self.ratingLbl.text = "\(self.rating) / \(self.cosmosview.settings.totalStars)"
                    
                }
            }
            else
            {
                print(error?.localizedDescription ?? "json error in getstarrating")
                self.cosmosview.rating = Double(self.rating)
                self.ratingLbl.text = "\(self.rating) / \(self.cosmosview.settings.totalStars)"
            }
        }
    }
    
    // Service Call for Create Recently Watched
    @objc func RecentlyWatched(withurl:String)
    {
      //  let parameters = ["createRecentlyWatched": ["videoId":TvshowPath["assetId"]! as AnyObject,"userId":userid as AnyObject]]
        
        let withurl = kRecentlyWatchedUrl + "appname=\(kAppName)&videoId=\(TvshowPath["assetId"]!)&token=\(userid)&seekTime=0"
    
        ApiManager.sharedManager.postDataWithJsonLambda(url: withurl, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                if JSON["statusCode"] != nil
                {
                    let status = JSON["statusCode"] as! Int
                    if status == 200
                    {
                        kisRecentlyUpdated = "RecentlyUpdated"
                        let result = JSON["result"] as! NSDictionary
                        let dicton = result["watchList"] as! NSDictionary
                        self.delegate?.recentlyWatcheddata(recentlyWatchedDict: dicton)
                        
                    }
                    else
                    {
                       print("json error in recently watched with status code of \(status)")
                    }
                }
                else
                
                {
                   print("json error in recently watched with out statusCode")
                }

            }
            else
            {
                print(error?.localizedDescription ?? "json error in recently watched call")
            }
        }
     /*   ApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters){(responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict
                kisRecentlyUpdated = "RecentlyUpdated"
                let dict = JSON as! NSDictionary
                let dicton = dict["recentlyWatched"] as! NSDictionary
//                if (dicton["seekTime"] as! Float64) > 0
//                {
//                    self.isRecentlywatch = true
//                }
                self.delegate?.recentlyWatcheddata(recentlyWatchedDict: dicton)
            }
            else
            {
//                print("json error")
//                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
//                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                    UIAlertAction in
//                let _ = self.navigationController?.popViewController(animated: true)
//                })
//                alertview.addAction(defaultAction)
//                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }*/
    }
    
    // Service call for ManageMyList
    @objc func ManageList(withurl:String,forPath:IndexPath,assetid:String)
    {
     //   let parameters = ["manageMyList":["videoId":(TvshowPath["assetId"])!as AnyObject,"userId":userid as AnyObject,"data":TvshowPath as AnyObject]]
    //    let lambdaparameters = ["appname":"BBF","model":"AppleTV4Gen","manufacturer":"Apple","device_name":"AppleTV","device_id": "","device_mac_address":"mac","brand_name":"Apple","host_name":"app","display_name":"apple","serial_number":"1234","version":"BBF"]
        let withurl = kManageListUrl + "appname=\(kAppName)&videoId=\(TvshowPath["assetId"]!)&token=\(userid)"
      
        var dict:Any!
        
        
        ApiManager.sharedManager.postDataWithJsonLambda(url: withurl, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                let cell = self.operationsListView.cellForRow(at: forPath)
                if JSON["statusCode"] != nil
                {
                    let status = JSON["statusCode"] as! Int
                    if status == 200
                    {
                       kisMyListUpdated = "MyListCalled"
                       let result = JSON["result"] as! NSDictionary
                        if result["data"] != nil
                        {
                            if result["data"] is NSArray
                            {
                                dict = result["data"] as! NSArray
                            }
                            else
                            {
                                dict = result["data"] as! NSDictionary
                            }
                           
                            if dict is NSArray
                            {
                                if ((dict as! NSArray).count != 0)
                                {
                                    (cell?.viewWithTag(11) as! UILabel).text = "Remove from MyList"
                                    (cell?.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList_hover")
                                    self.delegate?.myListdata(mylistDict: dict as! NSDictionary)
                                    (cell?.viewWithTag(9)!)?.isHidden = true
                                    
                                    
                                    self.delegate?.myListdata(mylistDict: dict as! NSDictionary)
                                }
                                else
                                {
                                    (cell?.viewWithTag(11) as! UILabel).text = "Add To MyList"
                                    (cell?.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist_hover")
                                    (cell?.viewWithTag(9)!)?.isHidden = true
                                    self.delegate?.removeListdata(id: assetid)
                                }
                            }
                            else
                            {
                                if ((dict as! NSDictionary).allKeys.count != 0)
                                {
                                    (cell?.viewWithTag(11) as! UILabel).text = "Remove from MyList"
                                    (cell?.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList_hover")
                                    (cell?.viewWithTag(9)!)?.isHidden = true
                                    
                                    self.delegate?.myListdata(mylistDict: dict as! NSDictionary)
                                }
                                else
                                {
                                    (cell?.viewWithTag(11) as! UILabel).text = "Add To MyList"
                                    (cell?.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist_hover")
                                    (cell?.viewWithTag(9)!)?.isHidden = true
                                    
                                    self.delegate?.removeListdata(id: assetid)
                                }
                            }
                        
                        }
                        else
                        {
                            (cell?.viewWithTag(11) as! UILabel).text = "Add To MyList"
                            (cell?.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist_hover")
                            (cell?.viewWithTag(9)!)?.isHidden = true
                             self.delegate?.removeListdata(id: assetid)
                        }
                    }
                    else
                    {
                        print("json error")
                    }
                }
                else
                {
                    print("jsonerror")
                }
            }
            else
            {
                print("json error")
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                    let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
       
       /* ApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters){(responseDict, error,isDone) in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                kisMyListUpdated = "MyListCalled"
                if JSON["myList"] != nil
                {
                    if JSON["myList"] is NSArray
                    {
                        dict = JSON["myList"] as! NSArray
                    }
                    else
                    {
                        dict = JSON["myList"] as! NSDictionary
                    }
                }
                let cell = self.operationsListView.cellForRow(at: forPath)
                if dict is NSArray
                {
                    if ((dict as! NSArray).count != 0)
                    {
                        (cell?.viewWithTag(11) as! UILabel).text = "Remove from MyList"
                        (cell?.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList_hover")
                        self.delegate?.myListdata(mylistDict: dict as! NSDictionary)
                        (cell?.viewWithTag(9)!)?.isHidden = true
                       
                        
                        self.delegate?.myListdata(mylistDict: dict as! NSDictionary)
                    }
                    else
                    {
                        (cell?.viewWithTag(11) as! UILabel).text = "Add To MyList"
                        (cell?.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist_hover")
                        (cell?.viewWithTag(9)!)?.isHidden = true
                       
                        
                    
                        self.delegate?.removeListdata(id: assetid)
                    }
                }
                else
                {
                    if ((dict as! NSDictionary)["data"] != nil)
                    {
                        (cell?.viewWithTag(11) as! UILabel).text = "Remove from MyList"
                        (cell?.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "removeList_hover")
                         (cell?.viewWithTag(9)!)?.isHidden = true
                  
                        self.delegate?.myListdata(mylistDict: dict as! NSDictionary)
                    }
                    else
                    {
                        (cell?.viewWithTag(11) as! UILabel).text = "Add To MyList"
                        (cell?.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "addlist_hover")
                        (cell?.viewWithTag(9)!)?.isHidden = true
                        
                        self.delegate?.removeListdata(id: assetid)
                    }
                }
                
                
            }
            else
            {
                print("json error")
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }*/
    }
    
   /* func getAssetData(withUrl:String,id:String,userid:String)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAssestData":["videoId":id as AnyObject,"userId":userid as AnyObject,"returnType":"tiny" as AnyObject]]
        ApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                let jsonresponse = JSON as! NSArray
                for dict in jsonresponse
                {
                    self.TvshowPath = dict as! NSDictionary
                }
            }
            else
            {
                print("json Error")
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
            self.operationsListView.reloadData()
        }
    }*/
    
    
    @objc func getuserRatingUpdate()
    {
       
        
      //  let parameters = ["userAssetRating": ["assetId": TvshowPath["assetId"] as AnyObject, "userId": gUser_ID as AnyObject, "rating":self.userRateValue,"previousRate":self.previousrate]]
  //     let url = "http://34.200.110.244:9000/userAssetRating"
       /* ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters as [String : [String : AnyObject]]){
            (responseDict,error,isDone) in
            if error == nil
            {
                self.userrating = self.userRateValue
                self.getrateUpdate()
               self.getStarrating()
            }
            else
            {
                print(error?.localizedDescription)
            }
        }*/
        
        let url = kgetUserratingUrl + "appname=\(kAppName)&token=\(self.userid)&assetId=\(TvshowPath["assetId"]!)&user_rating=\(self.userRateValue)&previousrate=\(self.previousrate)"
     
        ApiManager.sharedManager.postDataWithJsonLambda(url:url , parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                print("Iam in getuserrating method")
                print(self.userRateValue)
                print(self.previousrate)
                self.userrating = self.userRateValue
                self.getrateUpdate()
                self.getStarrating()
                self.isstarresponse = "true"
                self.ratingCell.isUserInteractionEnabled = true
            }
            else
            {
                self.isstarresponse = "true"
                self.ratingCell.isUserInteractionEnabled = true
                print("json error")
            }
        }
    
    }
    
    
    @objc func accountStatus()
    {
        
        let  parameters = [ "getAccountInfo": ["deviceId": self.deviceId!, "uuid": self.uuid!]]
        
        let url = kAccountInfoUrl + "appname=\(kAppName)&device_id=\(deviceId!)&uuid=\(uuid!)"
        
        ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters as [String : [String : AnyObject]]) {(responseDict , error,isDone) in
            if error == nil
            {
                let post = responseDict
                self.statusDict = post as! NSDictionary
                //                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                //                let accountInfo = storyBoard.instantiateViewController(withIdentifier: "Account") as! AccountInfoViewController
                //                accountInfo.accountDict = dict
                //                self.navigationController?.pushViewController(accountInfo, animated: true)
            }
            else
            {
                print("json error")
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                  let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func getassetdata(withUrl:String,id:String,userid:String)
    {
      //  var parameters =  [String:[String:AnyObject]]()
        print("Iam in play delegate getasset data")
 //       parameters = ["getAssetData":["videoId":id as AnyObject,"userId":userid as AnyObject]]
       
        let withUrl = kAssestDataUrl + "appname=\(kAppName)&assetId=\(id)&token=\(userid)"
        ApiManager.sharedManager.postDataWithJsonLambda(url:withUrl, parameters: ["appname":"\(kAppName)"])
            {
            (responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict
                if JSON is NSArray
                {
                    let dict = JSON as! NSArray
                    self.TvshowPath = dict.firstObject as! NSDictionary

                }
                else
                {
                    let dict = responseDict as! NSDictionary
                    if dict["statusCode"] != nil
                    {
                        let status = dict["statusCode"] as! Int
                        if status == 200
                        {
                            let result = dict["result"] as! NSDictionary
                            self.TvshowPath = result
                        }
                        else
                        {
                            print("json error")
                        }
                    }
                    else
                    {
                        print("json error")
                    }
                }
            self.viewWillAppear(true)

            }
            else
            {
               print("json error")
            }
            }
        

      /*  ApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                
                if JSON is NSArray
                {
                    let dict = JSON as! NSArray
                    self.TvshowPath = dict.firstObject as! NSDictionary
                }
                else
                {
                   self.TvshowPath = JSON as! NSDictionary
                }
                self.viewWillAppear(true)
                
            }
            else
            {
                print("json Error")
//                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
//                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                    UIAlertAction in
//                    let _ = self.navigationController?.popViewController(animated: true)
//                })
//                alertview.addAction(defaultAction)
//                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }*/
    }
}





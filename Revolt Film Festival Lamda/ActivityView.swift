/***
 **Module Name:  ActivityView
 **File Name :  ActivityView.swift
 **Project :   Revolt
 **Copyright(c) : Revolt.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Loader.
 */

import Foundation
import UIKit

class ActivityView: UIView
{
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.frame = frame
        self.backgroundColor = UIColor.darkGray
        self.alpha = 0.6
        var activityIndicator = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        activityIndicator.frame = CGRect(x: (frame.size.width-50)/2, y: (frame.size.height-50)/2, width: 50, height: 50)
        activityIndicator.startAnimating()
        self.addSubview(activityIndicator)
    }
    
    convenience init () {
        self.init(frame:CGRect.zero)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
}

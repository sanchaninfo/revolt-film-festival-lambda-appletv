/***
 **Module Name: ProductConfirmViewController.
 **File Name :  ProductConfirmViewController.swift
 **Project :   Revolt
 **Copyright(c) : Revolt.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Product Confirmation page.
 */

import UIKit
import Kingfisher

class StoreProductConfirmViewController: UIViewController {

    @objc var largeImageUrl = String()
    @objc var producttl,desc,price,quantity,tot,size : String!
    @objc var color:UIColor!
    @objc var userId = String()
    @objc var productID = String()
    @objc var colorCode = String()
    @objc var billArray = NSArray()
    @objc var totalPrice = String()
    @objc var uuid,deviceId:String!
    @objc var dataDict = NSDictionary()
    @IBOutlet weak var largeImage: UIImageView!
    @IBOutlet weak var selectClt: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var descTitle: UILabel!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var selectsize: UILabel!
    @IBOutlet weak var cnfBtn: UIButton!
    @IBOutlet weak var quan: UILabel!
    @objc var billAddress = NSArray()
    @objc var fromPlayLayer = Bool()
    override func viewDidLoad() {
       
        super.viewDidLoad()
      //  getaccountInfo()
        cnfBtn.layer.cornerRadius = 3.0
        largeImage.kf.setImage(with: URL(string: largeImageUrl))
        productTitle.text = producttl
        descTitle.text = desc
       // priceLbl.text = price
        quan.text = quantity
        let Qty = Int(quantity)!
        let tot1 = Float(tot)!
        let tot2 = (Float(Qty) * tot1)
        totalPrice = String(format: "%0.2f", tot2)
        
        selectsize.text = size
       // selectClt.layer.cornerRadius = min(selectClt.frame.size.height, selectClt.frame.size.width)/2.0
       // selectClt.clipsToBounds = true
        let attr = NSAttributedString(string: "$"  +  (String(format: "%0.2f", tot2)), attributes:[NSAttributedStringKey.foregroundColor:(UIColor.init(red: 200/255, green: 169/255, blue: 133/255, alpha: 1.0))])
        cnfBtn.setAttributedTitle(attr, for: .normal)
        selectClt.backgroundColor = color
        selectClt.layer.borderColor = focusColor
        selectClt.layer.borderWidth = 5.0
    }

    @IBAction func confirmBtn(_ sender: AnyObject) {
       // getFetchAddress()
        getaccountInfo()
    }
    @objc func getFetchAddress()
    {
     //   https://h15b1henj3.execute-api.us-east-1.amazonaws.com/FlixAPI/fetchaddressstore?appname=BBF&token=51d7d7c379752925324cb358f4b64ccae31fdce2d2b0df6768f0022ec7d92a34c60ac196c4ddc61acdad0300ab2191d0e930ada11279a77998ecf4f02d238d3eef1f349ea4aafa979badefdbf4a24a1fa98dc0c2cddecacddb8399ab4387d028e1009edefd3677e5e703f214a1bcb172e84f811b652af025594560984ed111dd646ea177b503e50fb0f91dfe432ad22917dee97549f236aeba38de9de75eedabf5dc68be726c6977b06184b776c05988859f1cdf2134e5dd040116fa2a8d0320978d4cccce3b75ea8b8a17478f837048a271adfb6e02114cc94ad94684892ace2c2412db3da80da2e2470645d6da
        
        let url = kFetchAddress + "appname=\(kAppName)&token=\(self.userId)"
        
        ApiManager.sharedManager.postDataWithJsonLambda(url: url, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
               let json = responseDict as! NSDictionary
               if json["statusCode"] != nil
               {
                   let status = json["statusCode"] as! Int
                   if status == 200
                   {
                      let result = json["result"] as! NSDictionary
                      if result.allKeys.count == 0
                      {
                        self.gotoNoBillPage()
                      }
                      else
                      {
                        self.billArray = result["address"] as! NSArray
                        self.gotoBillPage()
                      }
                    
                   }
                   else
                   {
                      print("json error")
                   }
               }
                else
               {
                  print("json error")
               }
            }
            else
            {
              print("json error")
            }
            
        }
      /*  let parameters = ["fetchAddressStore": ["user_id": userId]]
        ApiManager.sharedManager.postDataWithJson(url: kFetchAddress, parameters:parameters as [String : [String : AnyObject]]){
            (responseDict,error,isDone) in
            if error == nil
            {
                let JSON = responseDict as! NSArray
                if JSON.count == 0
                {
                    self.gotoNoBillPage()
                }
                else
                {
                let addressDict = JSON.firstObject as! NSDictionary
                self.billArray = addressDict["address"] as! NSArray
                self.gotoBillPage()
                }
            }
            else
            {
                print("json error")
            }
        }*/
    }
    
    @objc func gotoNoBillPage()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let NobillPage = storyBoard.instantiateViewController(withIdentifier: "NoBill") as! StoreNoBillViewController
        self.navigationController?.pushViewController(NobillPage, animated: true)
    }
    @objc func gotoBillPage()
    {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let billPage = storyBoard.instantiateViewController(withIdentifier: "Billing") as! StoreBillingViewController
            billPage.largeImageUrl = self.largeImageUrl
            billPage.productName = self.producttl
            billPage.size = self.size
            billPage.color = self.selectClt.backgroundColor!
            billPage.quantity = self.quantity
            billPage.total = self.totalPrice
            billPage.productID = self.productID
            billPage.colorCode = self.colorCode
            billPage.billArray = self.billArray
            billPage.userId = self.userId
            if dataDict.count != 0
            {
             billPage.accountDict = dataDict
             if fromPlayLayer
             {
              billPage.fromPlayLayer = true
            }
                else
             {
                billPage.fromPlayLayer = false
                }
             self.navigationController?.pushViewController(billPage, animated: true)
            }
            else
            {
                getaccountInfo()
            }
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedView == self.cnfBtn
        {
            cnfBtn.layer.borderWidth = 5.0
            cnfBtn.layer.borderColor = focusColor
        }
    }
    
    @objc func getaccountInfo()
    {
        let  parameters = [ "getAccountInfo": ["deviceId": deviceId!, "uuid": uuid!]]
      //  let url = kAccountInfoUrl + "appname=\(kAppName)&device_id=\(deviceId!)&uuid=\(uuid!)"
        let url = "http://34.200.110.244:9000/getAccountInfo"
        ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters as [String : [String : AnyObject]]) {(responseDict , error,isDone) in
            if error == nil
            {
                let post = responseDict
                let dict = post as! NSDictionary
                if (dict["uuid_exist"] as! Bool) == false
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.gotoCode()
                }
                else
                {
                    self.getFetchAddress()
                    self.dataDict = dict
                }
               
            }
            else
            {
                print(error?.localizedDescription ?? "account info error")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

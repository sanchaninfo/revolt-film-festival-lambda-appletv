/***
 **Module Name:  SearchList ViewController.
 **File Name :  SearchListViewController.swift
 **Project :   Revolt
 **Copyright(c) : Revolt.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Search Page.
 */

import UIKit

class SearchListViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UISearchResultsUpdating,UISearchBarDelegate {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchView: UIView!
    @objc var searchController : UISearchController!
    @objc var filteredDataItems = Array<Any>()
    @objc var searchCollectionList = NSMutableArray()
    @objc var userId = String()
    @objc var deviceId,uuid: String!
    var searchDelegate: myListdelegate!
    @objc var storeProdData = [[String:Any]]()
    @objc var Donatedict = [[String:Any]]()
    @objc var userData = [[String:Any]]()
    @objc var activityView = UIView()
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        searchBar.barTintColor = UIColor.white
        let lists:Array = searchCollectionList as Array
        let searchPredicate = NSPredicate(format: "assetName CONTAINS[C] %@", searchText)
        let array = (lists as NSArray).filtered(using: searchPredicate)
        filteredDataItems = array
        collectionView.reloadData()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.view.backgroundColor = UIColor.init(red: 13/255, green: 13/255, blue: 13/255, alpha: 1)
        searchController.searchBar.setScopeBarButtonTitleTextAttributes([NSAttributedStringKey.foregroundColor.rawValue:UIColor.white], for: .normal)
        searchController.searchBar.keyboardAppearance = UIKeyboardAppearance.dark
        searchController.searchBar.placeholder = NSLocalizedString("Enter keyword (e.g. The Secret to Ballin)", comment: "")
        let searchContainer = UISearchContainerViewController(searchController: searchController)
        searchContainer.title = NSLocalizedString("Search", comment: "")
        self.searchView.addSubview(searchContainer.view)
        self.addChildViewController(searchContainer)
       // filteredDataItems = searchCollectionList as Array
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return filteredDataItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let imagePath = filteredDataItems[indexPath.row] as! NSDictionary
        //  let image = imagePath["metadata"] as! NSDictionary
        (cell.viewWithTag(20) as! UILabel).text = (imagePath["assetName"] as! String)
        if (isnil(json: imagePath, key: kMovieart)) == ""
        {
            (cell.viewWithTag(21) as! UIImageView).image = UIImage(imageLiteralResourceName: "RevoltFlat")
        }
        else
        {
            (cell.viewWithTag(21) as! UIImageView).kf.setImage(with: URL(string:isnil(json: imagePath, key: kMovieart)))
        }
        if (imagePath["tve_provider_authorization"] != nil)
        {
            
            if (imagePath["tve_provider_authorization"] is NSString)
            {
                
                if (imagePath["tve_provider_authorization"] as! NSString == "true")
                {
                    if userData.count != 0
                    {
                        let data = userData.first! as NSDictionary
                        let provider = data["provider"] as! String
                        if provider == "" || provider == " "
                        {
                            (cell.viewWithTag(22) as! UIImageView).isHidden = false
                        }
                        else
                        {
                            (cell.viewWithTag(22) as! UIImageView).isHidden = true
                        }
                    }
                    
                }
                else
                {
                    (cell.viewWithTag(22) as! UIImageView).isHidden = true
                }
            }
            if (imagePath["tve_provider_authorization"] is Bool)
            {
                if (imagePath["tve_provider_authorization"] as! Bool == true)
                {
                    if userData.count != 0
                    {
                        let data = userData.first! as NSDictionary
                        let provider = data["provider"] as! String
                        if provider == "" || provider == " "
                        {
                            (cell.viewWithTag(22) as! UIImageView).isHidden = false
                        }
                        else
                        {
                            (cell.viewWithTag(22) as! UIImageView).isHidden = true
                        }
                    }
                }
                else
                {
                    (cell.viewWithTag(22) as! UIImageView).isHidden = true
                }
            }
            
        }
        else
        {
            (cell.viewWithTag(22) as! UIImageView).isHidden = true
        }

      return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        if let prev = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: prev)
        {
            cell.transform = .identity
        }
        if let next = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: next)
        {
            (cell.viewWithTag(21) as! UIImageView).adjustsImageWhenAncestorFocused = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let Path = filteredDataItems[indexPath.row] as! NSDictionary
        activityView = ActivityView.init(frame:(self.view?.frame)!)
        self.view?.addSubview(activityView)
        collectionView.isUserInteractionEnabled = false
        
        getaccountInfo(id:Path["assetId"] as! String,userid: userId/*,tvshow: (Path["tv_show"] as! Bool)*/)
        
       //  getAssetData(withUrl:kAssestDataUrl,id: Path["assetId"] as! String,userid: userId/*,tvshow:(Path["tv_show"] as! Bool)**/)
    }
    
    func updateSearchResults(for searchController: UISearchController)
    {
        searchController.searchBar.delegate = self
    }
    
   /* func getaccountInfo(id:String,userid:String/*tvshow:Bool*/)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAccountInfo":["deviceId":deviceId as AnyObject,"uuid":uuid as AnyObject]]
        let url = kAccountInfoUrl + "appname=\(kAppName)&device_id=\(deviceId!)&uuid=\(uuid!)"
        ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters){
            (responseDict,error,isDone) in
            if error == nil
            {
                let Json = responseDict
                let dict = Json as! NSDictionary
                accountresponse = (dict.value(forKey: "uuid_exist") as! Bool)
                if accountresponse == true
                {
                    self.getAssetData(withUrl:kAssestDataUrl,id: id,userid: userid/*,tvshow:tvshow*/)
                }
                else
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.gotoCode()
                }
            }
            else
            {
                print(error?.localizedDescription)
            }
        }
        
    }*/
    @objc func getaccountInfo(id:String,userid:String/*tvshow:Bool*/)
    {
//        var parameters =  [String:[String:AnyObject]]()
//        parameters = ["getAccountInfo":["deviceId":deviceId as AnyObject,"uuid":uuid as AnyObject]]
//        //   let url = kAccountInfoUrl + "appname=\(kAppName)&device_id=\(DeviceId!)&uuid=\(uuid!)"
//        let url = "http://34.200.110.244:9000/getAccountInfo"
//        ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters){
//            (responseDict,error,isDone) in
//            if error == nil
//            {
//                let Json = responseDict
//                let dict = Json as! NSDictionary
//
//                accountresponse = (dict.value(forKey: "uuid_exist") as! Bool)
//                if accountresponse == true
//                {
//                    self.getAssetData(withUrl:kAssestDataUrl,id: id,userid: userid/*tvshow:tvshow,*/)
//
//                }
//                else
//                {
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    appDelegate.gotoCode()
//                }
//            }
//        }
        
        let url = kAccountInfoUrl + "appname=\(kAppName)&token=\(self.userId)"
        ApiManager.sharedManager.postDataWithJsonLambda(url: url, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                let json = responseDict as! NSDictionary
                if json["statusCode"] != nil
                {
                    let status = json["statusCode"] as! Int
                    if status == 200
                    {
                        let result = json["result"] as! NSDictionary
                        accountresponse = result["uuid_exist"] as! Bool
                        if accountresponse == true
                        {
                           self.getAssetData(withUrl:kAssestDataUrl,id: id,userid: userid/*tvshow:tvshow,*/)
                        }
                        else
                        {
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.gotoCode()
                        }
                    }
                    else
                    {
                        print("json error")
                    }
                }
                else
                {
                    print("json error")
                }
                
            }
            else
            {
                print(error?.localizedDescription ?? "getaccountInfo got with error")
            }
        }
        
    }

    @objc func getAssetData(withUrl:String,id:String,userid:String/*,tvshow:Bool*/)
    {
  //      var parameters =  [String:[String:AnyObject]]()
     //   parameters = ["getAssetData":["videoId":id as AnyObject,"userId":userid as AnyObject]]
         let withUrl = kAssestDataUrl + "appname=\(kAppName)&assetId=\(id)&token=\(userid)"
       
        
        ApiManager.sharedManager.postDataWithJsonLambda(url: withUrl, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let DetailPage = storyBoard.instantiateViewController(withIdentifier: "DetailPage") as! DetailPageViewController
                DetailPage.userid = self.userId
                DetailPage.deviceId = self.deviceId
                DetailPage.uuid = self.uuid
                DetailPage.delegate = self.searchDelegate
                DetailPage.storeProdData = self.storeProdData
                DetailPage.Donatedict = self.Donatedict
                DetailPage.fromSearch = false
                
                if JSON is NSArray
                {
                    let dict = JSON as! NSArray
                    DetailPage.TvshowPath = dict.firstObject as! NSDictionary
                }
                else
                {
                   
                    let dict = responseDict as! NSDictionary
                    if dict["statusCode"] != nil
                    {
                        let status = dict["statusCode"] as! Int
                        if status == 200
                        {
                            let result = dict["result"] as! NSDictionary
                            DetailPage.TvshowPath = result
                            self.navigationController?.pushViewController(DetailPage, animated: true)
                        }
                        else
                        {
                            print("json error")
                        }
                    }
                    else
                    {
                        print("json error")
                    }
                }
            
                //  self.view.window = UIWindow(frame: UIScreen.main.bounds)
                
                // let nav1 = UINavigationController()
                // let mainView = ViewController(nibName: "Main", bundle: nil) //ViewController = Name of your controller
                //nav1.viewControllers = [mainView]
                //self.view.window!.rootViewController = nav1
                //  self.view.window?.makeKeyAndVisible()
                //  nav1.pushViewController(DetailPage, animated: true)
                //  self.present(DetailPage, animated: true, completion: nil)
                // self.navigationController?.pushViewController(DetailPage, animated: true)
                // self.navigationController?.pushViewController(DetailPage, animated: true)
                // self.navigationController?.present(DetailPage, animated: true, completion: nil)
                //  self.navigationController?.pushViewController(DetailPage, animated: true)
                
            }
            else
            {
                print(error?.localizedDescription ?? "getassetdata error")
            }
            self.collectionView.isUserInteractionEnabled = true
            self.activityView.removeFromSuperview()
            
        }
      /*  ApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let DetailPage = storyBoard.instantiateViewController(withIdentifier: "DetailPage") as! DetailPageViewController
                
                if JSON is NSArray
                {
                    let dict = JSON as! NSArray
                    DetailPage.TvshowPath = dict.firstObject as! NSDictionary
                }
                else
                {
                    DetailPage.TvshowPath = JSON as! NSDictionary
                }
                DetailPage.userid = self.userId
                DetailPage.deviceId = self.deviceId
                DetailPage.uuid = self.uuid
                DetailPage.delegate = self.searchDelegate
                DetailPage.storeProdData = self.storeProdData
                DetailPage.Donatedict = self.Donatedict
                DetailPage.fromSearch = false
              //  self.view.window = UIWindow(frame: UIScreen.main.bounds)
                
               // let nav1 = UINavigationController()
               // let mainView = ViewController(nibName: "Main", bundle: nil) //ViewController = Name of your controller
                //nav1.viewControllers = [mainView]
                //self.view.window!.rootViewController = nav1
              //  self.view.window?.makeKeyAndVisible()
              //  nav1.pushViewController(DetailPage, animated: true)
              //  self.present(DetailPage, animated: true, completion: nil)
               // self.navigationController?.pushViewController(DetailPage, animated: true)
              // self.navigationController?.pushViewController(DetailPage, animated: true)
               // self.navigationController?.present(DetailPage, animated: true, completion: nil)
              //  self.navigationController?.pushViewController(DetailPage, animated: true)
                self.navigationController?.pushViewController(DetailPage, animated: true)
              //  self.present(DetailPage, animated: true, completion: nil)
                
            }
            else
            {
                print(error?.localizedDescription)
//                print("json Error")
//                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
//                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
//                    UIAlertAction in
//                let _ = self.navigationController?.popViewController(animated: true)
//                })
//                alertview.addAction(defaultAction)
//                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
//extension SearchListViewController
//{
//    func myListdata(mylistDict: NSDictionary) {
//        
//    }
//    func removeListdata(id : String) {
//        
//    }
//    func recentlyWatcheddata(recentlyWatchedDict: NSDictionary) {
//        
//    }
//}

/***
 **Module Name:  StoreBillingController.
 **File Name : StoreBillingViewController.swift
 **Project :   Revolt
 **Copyright(c) : Revolt.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Store Billing.
 */
import UIKit

class StoreBillingViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {

    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var addressCollectionView: UICollectionView!
    @IBOutlet weak var rightArrow: UIImageView!
    @IBOutlet weak var leftArrow: UIImageView!

    
    @objc var billArray = NSArray()
    @objc var userId = String()
    @objc var shippingAddress = NSDictionary()
    @objc var billingAddress = NSDictionary()
    @objc var largeImageUrl = String()
    @objc var productName,size,quantity,total:String!
    @objc var color = UIColor()
    @objc var productID = String()
    @objc var orderDetails = NSDictionary()
    @objc var colorCode = String()
    @objc var accountDict = NSDictionary()
    @objc var fromPlayLayer = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.leftArrow.isHidden = true
        addressCollectionView.layer.borderWidth = 5
        addressCollectionView.layer.borderColor = UIColor.lightGray.cgColor
//        addressCollectionView.layer.shadowColor = UIColor.gray.cgColor
//        addressCollectionView.layer.shadowOffset = CGSize(width: 0, height: 10)
//        addressCollectionView.layer.shadowOpacity = 1
//        addressCollectionView.layer.shadowRadius = 10.0
//        addressCollectionView.layer.shadowPath = UIBezierPath(roundedRect: addressCollectionView.bounds, cornerRadius: 10).cgPath
//        addressCollectionView.clipsToBounds = false
//        addressCollectionView.layer.masksToBounds = false
        continueBtn.layer.cornerRadius = 5.0
            }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return billArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddressCell", for: indexPath)
        
        let isDefault = (billArray[indexPath.row] as! NSDictionary)["defaultAddress"] as! Bool
        let shippingAddress = ((billArray[indexPath.row] as! NSDictionary)["shippingAddress"] as! NSDictionary)
        let billingAddress = ((billArray[indexPath.row] as! NSDictionary)["billingAddress"] as! NSDictionary)
        
        let fullAddress = "\(shippingAddress["streetAddress"]!)" + " " + "\(shippingAddress["extendedAddress"]!)" + " " + "\(shippingAddress["locality"]!)" + " " + "\(shippingAddress["region"]!)" + " " + "\(shippingAddress["postalCode"]!)" + " " + "\(shippingAddress["countryCodeAlpha2"]!)"
        //  let fullAddress = "\(shippingAddress["streetAddress"]!)\n\(shippingAddress["extendedAddress"]!)\n\(shippingAddress["locality"]!)\n\(shippingAddress["region"]!)\n\(shippingAddress["postalCode"]!)\n\(shippingAddress["countryCodeAlpha2"]!)"
        
        //   let billingFullAddress = "\(billingAddress["streetAddress"]!)\n\(billingAddress["extendedAddress"]!)\n\(billingAddress["locality"]!)\n\(billingAddress["region"]!)\n\(billingAddress["postalCode"]!)\n\(billingAddress["countryCodeAlpha2"]!)"
        let billingFullAddress = "\(billingAddress["streetAddress"]!)" + " " + "\(billingAddress["extendedAddress"]!)" + " " + "\(billingAddress["locality"]!)" + " " + "\(billingAddress["region"]!)" + " " + "\(billingAddress["postalCode"]!)" + " " + "\(billingAddress["countryCodeAlpha2"]!)"
        var attributedText = NSMutableAttributedString()
       var attributedText1 = NSMutableAttributedString()
        if isDefault
        {
            attributedText = NSMutableAttributedString(string: "Shipping Address\n\(fullAddress)")
            attributedText.addAttribute(NSAttributedStringKey.font, value:UIFont.init(name: "Helvetica Bold", size: 35)!, range: NSMakeRange(0, ("Shipping Address").characters.count))
            attributedText1 = NSMutableAttributedString(string: "Billing Address\n\(fullAddress)")
            attributedText1.addAttribute(NSAttributedStringKey.font, value:UIFont.init(name: "Helvetica Bold", size: 35)!, range: NSMakeRange(0, ("Billing Address").characters.count))
            
            (cell.viewWithTag(10) as! UILabel).attributedText =  attributedText1
            (cell.viewWithTag(11) as! UILabel).attributedText = attributedText
        }
        else{
            
            attributedText = NSMutableAttributedString(string: "Shipping Address\n\(fullAddress)")
            attributedText.addAttribute(NSAttributedStringKey.font, value:UIFont.init(name: "Helvetica Bold", size: 35)!, range: NSMakeRange(0, ("Shipping Address").characters.count))
            attributedText1 = NSMutableAttributedString(string: "Billing Address\n\(billingFullAddress)")
            attributedText1.addAttribute(NSAttributedStringKey.font, value:UIFont.init(name: "Helvetica Bold", size: 35)!, range: NSMakeRange(0, ("Billing Address").characters.count))
            
            (cell.viewWithTag(10) as! UILabel).attributedText =  attributedText1
            (cell.viewWithTag(11) as! UILabel).attributedText = attributedText
        }
        
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        
        if let previousIndexPath = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: previousIndexPath)
        {
            cell.transform = CGAffineTransform.identity
        }
        if let indexPath = context.nextFocusedIndexPath,
            let  _ = collectionView.cellForItem(at: indexPath)
        {
            switch indexPath.row {
            case 0:
                self.leftArrow.isHidden = true
                self.rightArrow.isHidden = false
                break
            case (billArray.count-1):
                self.rightArrow.isHidden = true
                self.leftArrow.isHidden = false
                break
            default:
                self.rightArrow.isHidden = false
                self.leftArrow.isHidden = false
                break
            }
            billingAddress = ((billArray[indexPath.row] as! NSDictionary)["shippingAddress"] as! NSDictionary)
            shippingAddress = ((billArray[indexPath.row] as! NSDictionary)["billingAddress"] as! NSDictionary)

        }
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedView == continueBtn
        {
            continueBtn.layer.borderWidth = 5.0
            continueBtn.layer.borderColor = focusColor
        }
        if context.previouslyFocusedView == continueBtn
        {
            continueBtn.layer.borderWidth = 0.0
        }
    }
    
    @IBAction func continueAction(_ sender: Any) {
        getorderSummary()
    }
    
    @objc func getorderSummary()
    {
        let bill = ["firstName":billingAddress["firstName"] as! String,"lastName":billingAddress["lastName"]as! String,"streetAddress":billingAddress["streetAddress"]as! String,"extendedAddress":billingAddress["extendedAddress"]as! String,"locality":billingAddress["locality"]as! String,"region":billingAddress["region"]as! String,"postalCode":billingAddress["postalCode"]as! String,"countryCodeAlpha2":billingAddress["countryCodeAlpha2"]as! String]
    
        let ship = ["firstName":shippingAddress["firstName"]as! String,"lastName":shippingAddress["lastName"]as! String,"streetAddress":shippingAddress["streetAddress"]as! String,"extendedAddress":shippingAddress["extendedAddress"]as! String,"locality":shippingAddress["locality"]as! String,"region":shippingAddress["region"]as! String,"postalCode":shippingAddress["postalCode"]as! String,"countryCodeAlpha2":shippingAddress["countryCodeAlpha2"]as! String]
 
        let parameter = ["payWithCVV":["email":(accountDict.object(forKey: "email_id") as!String),"billingAddress":bill,"shippingAddress":ship,"userName":(accountDict.object(forKey: "full_name") as!String),"productName":self.productName,"productPrice":self.total,"productColor":self.colorCode,"productSize":self.size,"productQuantity":self.quantity,"productDeliveryCharge":"Free","userPhoneNumber":"","productImage":self.largeImageUrl,"productId":self.productID,"userId":userId,"customerId":(accountDict.object(forKey: "customer_id") as!String)]]
        
       // payment_method_nonce=9ae5e958-f5b9-0cc3-1d1c-c25590999766&email=siddartha.molleti@gmail.com&userName=siddartha molleti&productName=THUG LIFE BABY TEE&productPrice=35.99&clearancePrice=35.99&productSize=S&productQuantity=1&productImage=https://s3.amazonaws.com/peafowl/revoltfilmfestival/tees/large/THUG-LIFE-BABY-TEE.jpg&productDeliveryCharge=Free&userPhoneNumber=&shippingAddress_locality=Visakhpatnam&shippingAddress_postalCode=530001&shippingAddress_firstName=siddartha molleti&shippingAddress_lastName=&shippingAddress_streetAddress=Dwarakanagar&shippingAddress_extendedAddress=Akkayapalem&shippingAddress_region=AP&shippingAddress_countryCodeAlpha2=US&productId=1117&billingAddress_firstName=siddartha molleti&billingAddress_lastName=&billingAddress_streetAddress=Dwarakanagar&billingAddress_extendedAddress=Akkayapalem&billingAddress_locality=Visakhpatnam&billingAddress_region=AP&billingAddress_postalCode=530001&billingAddress_countryCodeAlpha2=US&productColor=%23000
        let clr = colorCode.replacingOccurrences(of: "#", with: "%23")
       
        let url = kPayCVV + "appname=\(kAppName)&token=\(self.userId)&email=\(accountDict.object(forKey: "email_id")as! String)&userName=\(accountDict.object(forKey: "full_name") as!String)&productName=\(self.productName!)&productPrice=\(self.total!)&clearancePrice=\(self.total!)&productSize=\(self.size!)&productQuantity=\(self.quantity!)&productImage=\(self.largeImageUrl)&productDeliveryCharge=Free&userPhoneNumber=&shippingAddress_locality=\(shippingAddress["locality"]as! String)&shippingAddress_postalCode=\(shippingAddress["postalCode"]as! String)&shippingAddress_firstName=\(shippingAddress["firstName"]as! String)&shippingAddress_lastName=\(shippingAddress["lastName"]as! String)&shippingAddress_streetAddress=\(shippingAddress["streetAddress"]as! String)&shippingAddress_extendedAddress=\(shippingAddress["extendedAddress"]as! String)&shippingAddress_region=\(shippingAddress["region"]as! String)&shippingAddress_countryCodeAlpha2=\(shippingAddress["countryCodeAlpha2"]as! String)&productId=\(self.productID)&billingAddress_firstName=\(billingAddress["firstName"] as! String)&billingAddress_lastName=\(billingAddress["lastName"]as! String)&billingAddress_streetAddress=\(billingAddress["streetAddress"]as! String)&billingAddress_extendedAddress=\(billingAddress["extendedAddress"]as! String)&billingAddress_locality=\(billingAddress["locality"]as! String)&billingAddress_region=\(shippingAddress["region"]as! String)&billingAddress_postalCode=\(billingAddress["postalCode"]as! String)&billingAddress_countryCodeAlpha2=\(billingAddress["countryCodeAlpha2"]as! String)&productColor=\(clr)"
       
        let mainurl = url.replacingOccurrences(of: " ", with: "%23")
      
        ApiManager.sharedManager.postDataWithJsonLambda(url: mainurl, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error ==  nil
            {
              let json = responseDict as! NSDictionary
                
                if json["statusCode"] != nil
                {
                    let status = json["statusCode"] as! Int
                    if status == 200
                    {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let orderPage = storyBoard.instantiateViewController(withIdentifier: "orderSummary") as! StoreorderSummaryViewController
                        orderPage.shippingAddress = self.shippingAddress
                        orderPage.billingAddress = self.billingAddress
                        orderPage.color = self.color
                        let result = json["result"] as! NSDictionary
                        orderPage.orderDetails = result
                        self.navigationController?.pushViewController(orderPage, animated: true)
                    }
                    else
                    {
                       print("json error")
                    }
                }
                else
                {
                    print("json error")
                }
            }
            else
            {
                print(error?.localizedDescription ?? "paywith cvv error")
                print("json error")
            }
        }
        
        
      /*  ApiManager.sharedManager.postDataWithJson(url:kPayCVV , parameters: parameter as [String : [String : AnyObject]]){
            (responseDict,error,isBool) in
            if error == nil
            {
                let json = responseDict as! NSDictionary
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let orderPage = storyBoard.instantiateViewController(withIdentifier: "orderSummary") as! StoreorderSummaryViewController
                orderPage.shippingAddress = self.shippingAddress
                orderPage.billingAddress = self.billingAddress
                orderPage.color = self.color
                orderPage.orderDetails = json
                if self.fromPlayLayer
                {
                    orderPage.fromPlayLayer = true
                }
                else
                {
                    orderPage.fromPlayLayer = false
                }
                self.navigationController?.pushViewController(orderPage, animated: true)
            }
            else
            {
                print("json error")
            }
            
        }*/
    }
    
}

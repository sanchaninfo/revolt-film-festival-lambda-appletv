/***
 **Module Name:  ApiManager
 **File Name :  ApiManager.swift
 **Project :   Revolt
 **Copyright(c) : Revolt.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : JSON parser using Alamofire.
 */

import Foundation
import UIKit
import Alamofire

class ApiManager
{
    static let sharedManager:ApiManager = ApiManager()
    func postDataWithJson(url:String,parameters:[String:[String:AnyObject]],completion:@escaping (_ responseDict:Any?,_ error:Error?,_ isDone:Bool)->Void)
    {
       // D3138795-8BD4-4619-9183-155500EE56A4
    //    let url = "https://h15b1henj3.execute-api.us-east-1.amazonaws.com/FlixAPI/getactivationcode?appname=BBF&host_name=app&brand_name=Apple&serial_number=1234&device_id=D3138795-8BD4-4619-9183-155500EE56A4&manufacturer=Apple&model=AppleTV4Gen"
     
//        let url = "https://h15b1henj3.execute-api.us-east-1.amazonaws.com/FlixAPI/getactivationcode?"
//        
//        let parameters = ["appname":"BBF","model":"AppleTV4Gen","manufacturer":"Apple","device_name":"AppleTV","device_id":"FBEEDA02-73A2-4EE4-B9EA-80EEBFB3F3F8","device_mac_address":"mac","brand_name":"Apple","host_name":"app","display_name":"apple","serial_number":"1234","version":"Revoltfilmfestival"]
       
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: nil).responseJSON { response in
            do
            {
                let post:Any = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
              
                completion(post,nil,true)
            }
            catch
            {
                completion(nil,response.result.error!,false)
               
            }
        }
    }
    
    func postDataWithJsonLambda(url:String,parameters:[String:String],completion:@escaping(_ responseDict:Any?,_ error:Error?,_ isDone:Bool)->Void)
    {
       
      //  let url = kActivationCodeUrl
          //   let url = "https://h15b1henj3.execute-api.us-east-1.amazonaws.com/FlixAPI/getactivationcode?appname=BBF&host_name=app&brand_name=Apple&serial_number=1234&device_id=D3138795-8BD4-4619-9183-155500EE56A4&manufacturer=Apple&model=AppleTV4Gen"
      Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.prettyPrinted, headers: nil).responseJSON { response in
                do
                {
                    let post:Any = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                    
                    completion(post,nil,true)
                }
                catch
                {
                    completion(nil,response.result.error!,false)
                    
                }
            }
    }
   
    
    
//    
//    func postDataWithJsonLambdawithUrl(url:NSURL,parameters:[String:String],completion:@escaping(_ responseDict:Any?,_ error:Error?,_ isDone:Bool)->Void)
//    {
//        
//        //  let url = kActivationCodeUrl
//        //   let url = "https://h15b1henj3.execute-api.us-east-1.amazonaws.com/FlixAPI/getactivationcode?appname=BBF&host_name=app&brand_name=Apple&serial_number=1234&device_id=D3138795-8BD4-4619-9183-155500EE56A4&manufacturer=Apple&model=AppleTV4Gen"
//        
//        Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.prettyPrinted, headers: nil).responseJSON { response in
//            do
//            {
//                let post:Any = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
//                
//                completion(post,nil,true)
//            }
//            catch
//            {
//                completion(nil,response.result.error!,false)
//                
//            }
//        }
//    }
    
    
    
    
    func getDatawithJsonLambda(url:String,completion: @escaping(_ responseDict:Any?)-> Void)
    {
        Alamofire.request(url).responseJSON{
            response in
            if response.response != nil
            {
                
                if response.response?.statusCode == 200
                {
                    completion(response.result.value)
                }
            }
            else
            {
                completion(nil)
            }
        }
    }
    
    func getDataWithJson(url:String,completion: @escaping(_ responseDict:Any?)-> Void)
    {
    
        Alamofire.request(url).responseJSON{
            response in
            if response.response != nil
            {
               
                if response.response?.statusCode == 200
                {
                    completion(response.result.value)
                }
            }
            else
            {
                completion(nil)
            }
        }
    }
}

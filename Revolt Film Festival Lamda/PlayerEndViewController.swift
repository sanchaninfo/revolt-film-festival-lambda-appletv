//
//  PlayerEndViewController.swift
//  DameDashStudios
//
//  Created by Sanchan on 03/03/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit

protocol playerEndDelegate:class{
    func playEnd(userId: String, videoId: String, deviceId: String, MyList: Bool,isfromplayend:Bool,storeData:[[String:Any]],DonateData:[[String:Any]])
}

class PlayerEndViewController: UIViewController {
    
    @objc var nextCollectionList = NSMutableArray()
    @objc var nextrowID = Int()
    @objc var userID = String()
    @objc var deviceID = String()
    @objc var nextData = NSDictionary()
    @objc var videoDict = NSDictionary()
    @objc var DonateData = [[String:Any]]()
    @objc var storeData = [[String:Any]]()
    @objc var frommylist = Bool()

    @IBOutlet weak var bgImg: UIImageView!
  
    @IBOutlet weak var titleLbl: UILabel!
   
    @IBOutlet weak var automated: UILabel!
    @IBOutlet weak var desLbl: UILabel!
    @IBOutlet weak var thumbImg: UIImageView!

    @IBOutlet weak var duration: UILabel!
    @objc var myTimer = Timer()
    @objc var i = Int()
    var playEnddelegate:playerEndDelegate?
    @objc var videoUrl = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        i = 5
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleMenuPress))
        tapGesture.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
        self.view.addGestureRecognizer(tapGesture)
   
      
        let bgImage = isnil(json: nextData, key: "banner_image_1300x650")
        if bgImage == "" || bgImage == " "
        {
            bgImg.image = UIImage(imageLiteralResourceName: "mainCarouselImg")
        }
        else
        {
            bgImg.kf.setImage(with: URL(string: bgImage))
            
        }
        let thumbImage = isnil(json: nextData, key: kMovieart)
        if thumbImage == "" || thumbImage == " "
        {
            thumbImg.image = UIImage(imageLiteralResourceName: "RevoltFlat")
        }
        else
        {
            thumbImg.kf.setImage(with: URL(string: thumbImage))
        }
        
       
        titleLbl.text = isnil(json: nextData, key: "assetName")
        desLbl.text = isnil(json: nextData, key: "description")
        let dur = isnil(json: nextData, key: "fileSize")
        let time1 = Double(dur)
        if time1 != nil
        {
            duration.text =  stringFromTimeInterval(interval: time1!)
        }
        else
        {
            duration.text = "0 M"
        }
       
    }

    @objc func handleMenuPress()
    {
        for viewcontroller in (self.navigationController?.viewControllers)!
        {
            if viewcontroller.isKind(of: DetailPageViewController.self)
            {
                 let  _ =  self.navigationController?.popToViewController(viewcontroller, animated: false)
            }
        }
    }
    @objc func getData(getnextData:NSDictionary)
    {
        self.nextData = getnextData
  
        RecentlyWatched(withurl:kRecentlyWatchedUrl)
        myTimer = Timer.scheduledTimer(timeInterval: 1.1, target: self, selector: #selector(self.getplay), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        i = 5
    }
    @objc func getplay()
    {
   
        automated.text = "\(String(i)) SEC"
        i = i-1
        if i == 0
        {
          myTimer.invalidate()
          print("player is ready")
          playvideo()
        }
        
    }
    
    @IBAction func playButton(_ sender: Any) {
        myTimer.invalidate()
        playvideo()
    }
    
    @objc func playvideo()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let PlayerVC = storyBoard.instantiateViewController(withIdentifier: "playerLayer") as! PlayerLayerViewController
        var monotize = String()
        var monotizeLabel = String()
        var montizetype = String()
        if nextData.object(forKey: "metadata") != nil
        {
            let metaDict = nextData.object(forKey: "metadata") as! NSDictionary
            if metaDict["monetize"] != nil && metaDict["monetizeLabel"] != nil && metaDict["monetizeType"] != nil
            {
                if metaDict["monetize"] is String
                {
                    monotize = metaDict["monetize"] as! String
                }
                else
                {
                    monotize = ""
                }
                if metaDict["monetizeLabel"] is String
                {
                    monotizeLabel = metaDict["monetizeLabel"] as! String
                }
                else
                {
                    monotizeLabel = ""
                }
                if metaDict["monetizeType"] is String
                {
                    montizetype = metaDict["monetizeType"] as! String
                }
                else
                {
                    montizetype = ""
                }

            }
        }
        else
        {
            if nextData["monetize"] != nil && nextData["monetizeLabel"] != nil && nextData["monetizeType"] != nil
            {
                if nextData["monetize"] is String
                {
                    monotize = nextData["monetize"] as! String
                }
                else
                {
                    monotize = ""
                }
                if nextData["monetizeLabel"] is String
                {
                    monotizeLabel = nextData["monetizeLabel"] as! String
                }
                else
                {
                    monotizeLabel = ""
                }
                if nextData["monetizeType"] is String
                {
                    montizetype = nextData["monetizeType"] as! String
                }
                else
                {
                    montizetype = ""
                }
            }
          
            
        }
       
            PlayerVC.isResume = false
         
            if nextData["m3u8_url"] != nil && nextData["m3u8_url"] as! String != ""
            {
               videoUrl = nextData["m3u8_url"] as! String
            
            }
            else
            {
                if nextData["url"] != nil && nextData["url"] as! String != ""
                {
                  videoUrl = nextData["url"] as! String
                }
                else
                {
                    if nextData["mp4_url"] != nil && nextData["mp4_url"] as! String != ""
                    {
                     videoUrl = nextData["mp4_url"] as! String
                    }
                }
                
            }
            PlayerVC.videoUrl = self.videoUrl
            PlayerVC.mainVideoID = nextData["assetId"] as! String
            PlayerVC.userID = userID
            PlayerVC.deviceID = deviceID
            PlayerVC.videoID = (nextData["assetId"] as! String)
            PlayerVC.isfromMylist = self.frommylist
            PlayerVC.isMyList = self.frommylist
            PlayerVC.monitizeLbl = monotizeLabel
            PlayerVC.monitizetype = montizetype
            if monotize == "true"
            {
            PlayerVC.DonateData = self.DonateData
            PlayerVC.isDonate = true
            }
            else
            {
            PlayerVC.storeData = self.storeData
            PlayerVC.isDonate = false
            }
            PlayerVC.isfromplayend = true
        self.playEnddelegate?.playEnd(userId: self.userID, videoId: nextData["assetId"] as! String, deviceId: self.deviceID, MyList: self.frommylist,isfromplayend:true,storeData:self.storeData,DonateData:self.DonateData)
        self.navigationController?.pushViewController(PlayerVC, animated: true)
       //     let _ = self.navigationController?.popViewController(animated: true)
        
    }
    @objc func RecentlyWatched(withurl:String)
    {
     //   let parameters = ["createRecentlyWatched": ["videoId":nextData["assetId"] as! String,"userId":userID as AnyObject]]
  
        let withurl = kRecentlyWatchedUrl + "appname=\(kAppName)&videoId=\(nextData["assetId"]!)&token=\(userID)&seekTime=0"
        print(withurl)
        ApiManager.sharedManager.postDataWithJsonLambda(url: withurl, parameters: ["appname":"revoltfilm"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                if JSON["statusCode"] != nil
                {
                    let status = JSON["statusCode"] as! Int
                    if status == 200
                    {
                        let result = JSON["result"] as! NSDictionary
                       _ = result["watchList"] as! NSDictionary
            
                    }
                    else
                    {
                        print("json error in recently watched with status code of \(status)")
                    }
                }
                else
                    
                {
                    print("json error without statuscode flag")
                }
                
            }
            else
            {
                print(error?.localizedDescription ?? "json error in recently watched call")
            }
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

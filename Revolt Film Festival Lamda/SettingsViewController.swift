/***
 **Module Name:  SettingsViewController.
 **File Name : SettingsViewController.swift
 **Project :   Revolt
 **Copyright(c) : Revolt.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : setting Page.
 */

import UIKit

class SettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @objc var menu = [String]()
    @objc var activityView = UIView()
    @objc var deviceId,uuid,userId : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        menu.append("My Account")
        menu.append("Get help")
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        (cell.viewWithTag(11) as! UILabel).text = menu[indexPath.row]
        cell.layer.cornerRadius = 7.0
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if let previous = context.previouslyFocusedIndexPath,
            let cell = tableView.cellForRow(at: previous)
        {
            (cell.viewWithTag(11) as! UILabel).textColor = UIColor.white
        }
        if let next = context.nextFocusedIndexPath,
            let cell = tableView.cellForRow(at: next)
        {
            (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            accountstatus()
            activityView = ActivityView.init(frame:(self.view.frame))
            self.view.addSubview(activityView)
        }
    }
    
    @objc func accountstatus()
    {

       //   let  parameters = [ "getAccountInfo": ["deviceId": deviceId!, "uuid": uuid!]]
          let url = kAccountInfoUrl + "appname=\(kAppName)&token=\(self.userId!)"

        
        ApiManager.sharedManager.postDataWithJsonLambda(url: url, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict , error,isDone) in
            if error ==  nil
            {
                let post = responseDict
                let dict = post as! NSDictionary
                if dict["statusCode"] != nil
                {
                   let status = dict["statusCode"] as! Int
                    if status == 200
                    {
                      let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                      let result = dict["result"] as! NSDictionary
                      let accountInfo = storyBoard.instantiateViewController(withIdentifier: "Account") as! AccountInfoViewController
                      accountInfo.accountDict = result
                        if (result["uuid_exist"] as! Bool) == true
                        {
                            self.navigationController?.pushViewController(accountInfo, animated: true)
                        }
                        else
                        {
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.gotoCode()
                        }
                    }
                    else
                    {
                    print("json error")
                    }
                    
                }
                else
                {
                   print("json error")
                }
                
            }
            else
            {
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                    let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
            self.activityView.removeFromSuperview()
        }
      
     /*   let url = "http://34.200.110.244:9000/getAccountInfo"
       ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters as [String : [String : AnyObject]]) {(responseDict , error,isDone) in
            if error == nil
            {
                let post = responseDict
                let dict = post as! NSDictionary
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let accountInfo = storyBoard.instantiateViewController(withIdentifier: "Account") as! AccountInfoViewController
                accountInfo.accountDict = dict
           
                if (dict["uuid_exist"] as! Bool) == true
                {
                    self.navigationController?.pushViewController(accountInfo, animated: true)
                }
                else
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.gotoCode()
                }
        }
            else
            {
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                   let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

/***
 **Module Name:  CategoriesViewController
 **File Name :  CategoriesViewController.swift
 **Project :   Revolt
 **Copyright(c) : Revolt.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : categories page.
 */

import UIKit
import Cosmos

class CategoriesViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    @objc var CarousalData = [[String:Any]]()
    @objc var CategoryCarousalName = NSMutableArray()
    @objc var menuCollectionList = NSMutableArray()
    @objc var UserId,uuid,deviceId:String!
    @objc var MyListthumb = [[String:Any]]()
    var categoryDelegate:myListdelegate!
    @objc var insetButton : UIButton = UIButton()
    @objc var nextButton: UIButton = UIButton()
    @objc var PhotoDict = [[String:Any]]()
    @objc var storeProdData = [[String:Any]]()
    @objc var isPhoto = Bool()
    @objc var isstore = Bool()
    @objc var tableindex = Int()
    @objc var storeProdDict = [[String:Any]]()
    @objc var Donatedict = [[String:Any]]()
    @objc var userData = [[String:Any]]()
    @objc var activityView = UIView()
    

    private  var rightHandFocusGuide = UIFocusGuide()
    
    @IBOutlet var likeView: UIView!
   
    @IBOutlet weak var cosmosview: CosmosView!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var listcollectionview: UICollectionView!
    @IBOutlet weak var backgroundimg: UIImageView!
    @IBOutlet weak var focusButton: UIButton!
    @IBOutlet weak var textView: UIView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieYear: UILabel!
    @IBOutlet weak var movieTime: UILabel!
    @IBOutlet weak var rightHandView:UIView!
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet var lftImg: UIImageView!
    @IBOutlet var Btn: UIButton!
    @objc var viewToFocus: UIView? = nil
    {
        didSet
        {
            if viewToFocus != nil
            {
                self.setNeedsFocusUpdate();
                self.updateFocusIfNeeded();
            }
        }
    }
    override weak var preferredFocusedView: UIView?
    {
        if viewToFocus != nil
        {
            return viewToFocus;
        }
        else
        {
            return super.preferredFocusedView
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Btn.isHidden = true
        Btn.isEnabled = false
        lftImg.isHidden = true
      //  likeView.isHidden = true
        cosmosview.isHidden = true
        cosmosview.settings.fillMode = .precise
        insetButton.frame = CGRect(x: 0, y: 630, width: 10, height: 10)
        insetButton.backgroundColor = UIColor.clear
        insetButton.layer.borderWidth = 1.0
        insetButton.layer.borderColor = UIColor.clear.cgColor
        insetButton.setTitle("Press This", for: .normal)
        insetButton.setTitle("Highlighted This", for: .highlighted)
        insetButton.setTitle("Selected This", for: .selected)
        insetButton.setTitle("Focused This", for: .focused)
        insetButton.setTitleColor(UIColor.clear, for: .normal)
        rightHandView.addSubview(insetButton)
        //        nextButton.frame = CGRect(x: 0, y: 830, width: 10, height: 10)
        //        nextButton.backgroundColor = UIColor.clear
        //        nextButton.layer.borderWidth = 1.0
        //        nextButton.layer.borderColor = UIColor.clear.cgColor
        //        rightHandView.addSubview(nextButton)
        view.addLayoutGuide(rightHandFocusGuide)
        
        rightHandFocusGuide.bottomAnchor.constraint(equalTo: tableview.bottomAnchor).isActive = true
        rightHandFocusGuide.leftAnchor.constraint(equalTo: tableview.rightAnchor).isActive = true
        rightHandFocusGuide.topAnchor.constraint(equalTo: tableview.topAnchor).isActive = true
        rightHandFocusGuide.rightAnchor.constraint(equalTo: rightHandView.leftAnchor).isActive = true
        rightHandFocusGuide.preferredFocusedView = listcollectionview
        textView.isHidden = true
        textView.isUserInteractionEnabled = false
    }
    
    //Number of section
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    //Number of rows in a Section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return CategoryCarousalName.count
    }
    
    //Cell Item at IndexPath
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        (cell.viewWithTag(10) as! UILabel).text = (CategoryCarousalName[indexPath.row] as! String)
        //  cell.textLabel?.text = CategoryCarousalName[indexPath.row] as? String
        //  cell.textLabel?.font = UIFont.init(name: "Helvetica", size: 29.0)
        //  cell.textLabel?.textColor = UIColor.white
        cell.selectionStyle = .none
        cell.layer.cornerRadius = 8.0
        return cell
    }
    
    //Did Update Focus
    func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        guard let nextView = context.nextFocusedView else { return }
        if (nextView == insetButton)
        {
            guard let indexPath = context.previouslyFocusedIndexPath, let cell = tableView.cellForRow(at: indexPath) else {
                return
            }
            (cell.viewWithTag(10) as! UILabel).textColor = UIColor.white
            // cell.textLabel?.textColor = UIColor.white
            //  rightHandFocusGuide.preferredFocusedView = cell
            viewToFocus = listcollectionview
           // tableView.frame = CGRect(x: tableView.frame.origin.x - 565, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.frame.size.height)
            tableView.isHidden = true
            tableindex = indexPath.row
            rightHandView.frame = CGRect(x: rightHandView.frame.origin.x - 300, y: rightHandView.frame.origin.y  , width: rightHandView.frame.size.width, height: rightHandView.frame.size.height)
            Btn.isHidden = false
            lftImg.isHidden = false
            Btn.isEnabled = true
            insetButton.isEnabled = false
            
        }
        else
        {
            //            if nextView == Btn
            //            {
            
            //            if nextView == Btn
            //            {
            //                rightHandView.frame = CGRect(x: rightHandView.frame.origin.x, y: rightHandView.frame.origin.y  , width: rightHandView.frame.size.width, height: rightHandView.frame.size.height)
            //                tableView.isHidden = false
            //                rightHandFocusGuide.preferredFocusedView = insetButton
            //            }
            //            else
            //            {
            
            rightHandView.frame = CGRect(x: rightHandView.frame.origin.x, y: rightHandView.frame.origin.y  , width: rightHandView.frame.size.width, height: rightHandView.frame.size.height)
            tableView.isHidden = false
           // tableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.frame.size.height)
            rightHandFocusGuide.preferredFocusedView = insetButton
            // }
            //   }
            
        }
        guard let indexPath = context.nextFocusedIndexPath, let cell = tableView.cellForRow(at: indexPath)
            else { return }
        //    cell.textLabel?.textColor = UIColor.black
        (cell.viewWithTag(10) as! UILabel).textColor = UIColor.black
        
//        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
//        swipeRight.direction = UISwipeGestureRecognizerDirection.right
//        cell.viewWithTag(10)!.addGestureRecognizer(swipeRight)
        
        
        
        let selectedCarousal = self.CategoryCarousalName[(indexPath as NSIndexPath).row] as! String
        if selectedCarousal == "Photos"
        {
            isPhoto = true
            isstore = false
            categoriesSelected(carousalName: selectedCarousal, carousalDetailDict: PhotoDict)
        }
        else if selectedCarousal == "Store"
        {
             isstore = true
            isPhoto = false
            categoriesSelected(carousalName: selectedCarousal, carousalDetailDict: storeProdData)
        }
        else
        {
            isPhoto = false
            isstore = false
            categoriesSelected(carousalName: selectedCarousal,carousalDetailDict: CarousalData)
            
        }
        guard let prevIndexPath = context.previouslyFocusedIndexPath, let prevCell = tableView.cellForRow(at: prevIndexPath)
            else { return }
        // prevCell.textLabel?.textColor = UIColor.white
        (prevCell.viewWithTag(10) as! UILabel).textColor = UIColor.white
        textView.isHidden = true
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedView == Btn
        {
            rightHandView.frame = CGRect(x: rightHandView.frame.origin.x + 300, y: rightHandView.frame.origin.y  , width: rightHandView.frame.size.width, height: rightHandView.frame.size.height)
          //  tableview.frame = CGRect(x:tableview.frame.origin.x + 565 , y: tableview.frame.origin.y, width: tableview.frame.size.width, height: tableview.frame.size.height)
            tableview.isHidden = false
            Btn.isHidden = true
            lftImg.isHidden = true
            viewToFocus = tableview.cellForRow(at: IndexPath(row: tableindex, section: 0))
            insetButton.isEnabled = true
            movieYear.text = ""
            movieTime.text = ""
            movieTitle.text = ""
       //     likeView.isHidden = true
            cosmosview.isHidden = true
            ratingLbl.isHidden = true
            // rightHandFocusGuide.preferredFocusedView = tableview
        }
//        if context.nextFocusedView == insetButton
//        {
//            viewToFocus = listcollectionview
//            tableview.frame = CGRect(x: tableview.frame.origin.x - 565, y: tableview.frame.origin.y, width: tableview.frame.size.width, height: tableview.frame.size.height)
//            //  tableView.isHidden = true
//          //  tableindex = indexPath.row
//            rightHandView.frame = CGRect(x: rightHandView.frame.origin.x - 300, y: rightHandView.frame.origin.y  , width: rightHandView.frame.size.width, height: rightHandView.frame.size.height)
//            Btn.isHidden = false
//            lftImg.isHidden = false
//            Btn.isEnabled = true
//            insetButton.isEnabled = false
//        }
        
    }
    
    // Number of section in listcollectionview
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    // Number of rows in listcollectionview
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuCollectionList.count
    }
    
    //Cell for Item in listcollectionview
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell1" , for: indexPath)
        let imagePath = menuCollectionList[indexPath.row] as! NSDictionary
        if isPhoto == true
        {
            (cell.viewWithTag(10) as! UIImageView).kf.indicatorType = .activity
            (cell.viewWithTag(10) as! UIImageView).kf.setImage(with: URL(string: (kPhotoBaseUrl + (imagePath["thumb"] as! String))))
        }
            
        else if isstore == true
        {
            (cell.viewWithTag(10) as! UIImageView).kf.indicatorType = .activity
            (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "StoreIcon")
        }
        else
        {
            let img = (cell.viewWithTag(10) as! UIImageView)
            img.kf.indicatorType = .activity
            if (isnil(json: imagePath, key: kMovieart)) == ""
            {
                (cell.viewWithTag(10) as! UIImageView).image = UIImage(imageLiteralResourceName: "RevoltFlat")
            }
            else
            {
                (cell.viewWithTag(10) as! UIImageView).kf.setImage(with: URL(string:isnil(json: imagePath, key: kMovieart)))
            }
            
            if (imagePath["tve_provider_authorization"] != nil)
            {
                
                if (imagePath["tve_provider_authorization"] is NSString)
                {
                    if (imagePath["tve_provider_authorization"] as! NSString == "true")
                    {
                        if userData.count != 0
                        {
                            let data = userData.first! as NSDictionary
                            let provider = data["provider"] as! String
                            if provider == "" || provider == " "
                            {
                                (cell.viewWithTag(11) as! UIImageView).isHidden = false
                            }
                            else
                            {
                                (cell.viewWithTag(11) as! UIImageView).isHidden = true
                            }
                        }
                        
                    }
                    else
                    {
                        (cell.viewWithTag(11) as! UIImageView).isHidden = true
                    }
                }
                if (imagePath["tve_provider_authorization"] is Bool)
                {
                    if (imagePath["tve_provider_authorization"] as! Bool == true)
                    {
                        if userData.count != 0
                        {
                            let data = userData.first! as NSDictionary
                            let provider = data["provider"] as! String
                            if provider == "" || provider == " "
                            {
                                (cell.viewWithTag(11) as! UIImageView).isHidden = false
                            }
                            else
                            {
                                (cell.viewWithTag(11) as! UIImageView).isHidden = true
                            }
                        }
                        
                    }
                    else
                    {
                        (cell.viewWithTag(11) as! UIImageView).isHidden = true
                    }
                }
                
            }
            else
            {
                (cell.viewWithTag(11) as! UIImageView).isHidden = true
            }
        }
        
        switch indexPath.row
        {
        case 0:
            if isPhoto == true
            {
                textView.isHidden = false
                backgroundimg.kf.indicatorType = .activity
                backgroundimg.kf.setImage(with: URL(string: (kPhotoBaseUrl + (imagePath["large"] as! String))))
                
                /*   movieTitle.text = (imagePath["title"] as! String)
                 movieYear.text = ""
                 movieTime.text = ""*/
            }
            else if isstore == true
            {
                textView.isHidden = false
                backgroundimg.kf.indicatorType = .activity
                backgroundimg.kf.setImage(with: URL(string: imagePath["posterURL"] as! String))
                /* let Path = storeProdData.first! as NSDictionary
                 let carousel = Path["carousels"] as! NSArray
                 let data = carousel.firstObject as! NSDictionary
                 movieTitle.text = (data["carousel"] as! String)
                 movieYear.text = ""
                 movieTime.text = (Path["duration"] as! String)*/
            }
            else
            {
                textView.isHidden = false
                backgroundimg.kf.setImage(with: URL(string: isnil(json: imagePath, key: "banner_image_1300x650")))
                
            }
            
            break
        default:
            break
        }
        return cell

    }
    
    
    func collectionView(_ collectionView: UICollectionView, canFocusItemAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    // listcollectio view update focus
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        textView.isHidden = false
        if let previousIndexPath = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: previousIndexPath)
        {
            //  let imagePath = menuCollectionList[previousIndexPath.row] as! NSDictionary
            // let image = imagePath["metadata"] as! NSDictionary
            //  backgroundimg.kf.setImage(with: URL(string: isnil(json: image, key: "main_carousel_image_url")))
            cell.transform = CGAffineTransform.identity
        }
        if let indexPath = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: indexPath)
        {
     
            cosmosview.isHidden = false
            ratingLbl.isHidden = false
            let imagePath = menuCollectionList[indexPath.row] as! NSDictionary
          
            if isPhoto == true
            {
                textView.isHidden = false
                backgroundimg.kf.indicatorType = .activity
                backgroundimg.kf.setImage(with: URL(string: (kPhotoBaseUrl + (imagePath["large"] as! String))))
                (cell.viewWithTag(10) as! UIImageView).adjustsImageWhenAncestorFocused = true
                movieTitle.text = (imagePath["title"] as! String)
                movieYear.text = ""
                movieTime.text = ""
            }
            else if isstore == true
            {
                textView.isHidden = false
                backgroundimg.kf.indicatorType = .activity
                cosmosview.isHidden = true
                (cell.viewWithTag(10) as! UIImageView).adjustsImageWhenAncestorFocused = true
                let Path = storeProdData.first! as NSDictionary
                let carousel = Path["carousels"] as! NSArray
                let data = carousel.firstObject as! NSDictionary
                //   print(Path)
                //  backgroundimg.kf.setImage(with: URL(string: imagePath["posterURL"] as! String))
                movieTitle.text = (data["carousel"] as! String).capitalized
                movieYear.text = ""
                movieTime.text = (Path["duration"] as! String)
                
            }
            else
            {
                textView.isHidden = false
               
                backgroundimg.kf.setImage(with: URL(string: isnil(json: imagePath, key: "banner_image_1300x650")))
                collectionView.scrollToItem(at: indexPath, at: [.centeredHorizontally, .centeredVertically], animated: true)
                (cell.viewWithTag(10) as! UIImageView).adjustsImageWhenAncestorFocused = true
                movieTitle.text = (isnil(json: imagePath, key: "assetName")).capitalized
                movieTitle?.frame = CGRect(x: (movieTitle?.frame.origin.x)!, y: (movieTitle?.frame.origin.y)!, width: ((movieTitle.text)?.widthWithConstrainedWidth(height: 60, font: (movieTitle?.font)!))!, height: (movieTitle?.frame.size.height)!)
                let str1 = ((isnil(json: imagePath, key: "releaseDate")).components(separatedBy: "-"))
                movieYear.text = str1[0]
                let time = isnil(json:imagePath,key:"fileSize")
                
                let time1 = Double(time)
                if time1 != nil
                {
                    movieTime.text =  stringFromTimeInterval(interval: time1!)
                }
                else
                {
                    movieTime.text = "0 M"
                }
                getStarrating(assetId: imagePath["assetId"] as! String, UserId: self.UserId)

            }

        }
    }
    
    // did select list collection view
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let path = menuCollectionList[indexPath.row] as! NSDictionary
        if isPhoto == true
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let PhotoPage = storyBoard.instantiateViewController(withIdentifier: "Photos") as! PhotosViewController
            PhotoPage.PhotoDict = self.PhotoDict
            PhotoPage.ImageIndexpath = indexPath.row
            self.navigationController?.pushViewController(PhotoPage, animated: true)
        }
        else if isstore == true
        {
            let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
            let StorePage = storyBoard.instantiateViewController(withIdentifier: "Store") as! StoreViewController
            let storeDict = storeProdData.first! as NSDictionary
            StorePage.prodData = storeDict
            StorePage.userId = UserId
            StorePage.uuid = uuid
            StorePage.deviceID = deviceId
            self.navigationController?.pushViewController(StorePage, animated: true)
        }
        else
        {
            activityView = ActivityView.init(frame:(self.view?.frame)!)
            self.view?.addSubview(activityView)
            self.listcollectionview.isUserInteractionEnabled = false
            
            
            getaccountInfo(id: path["assetId"] as! String, userid: self.UserId)
        //    getAssetData(withUrl:kAssestDataUrl,id: path["assetId"] as! String,userid: UserId)
            
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberofcolumns = CGFloat(4)
        let totalwidth = listcollectionview.frame.width
        let itemWidth = ((totalwidth - (numberofcolumns - 1)) / numberofcolumns)
        return CGSize(width: itemWidth, height: 230)
    }
    
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped Right")
//                if self.userRateValue >= 0 && self.userRateValue < 6
//                {
//                    //  self.userrating = self.userrating + 1
//                    self.userRateValue = self.userRateValue + 1
//                    getrateUpdate()
//                }
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped Left")
//                if self.userRateValue > 0  && self.userRateValue <= 6
//                {
//                    //  self.userrating = self.userrating - 1
//                    self.userRateValue = self.userRateValue - 1
//                    getrateUpdate()
//                }
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    
    
    // get star rating
    
    @objc func getStarrating(assetId:String,UserId:String)
    {
       // let parameters = ["getAssetRating":["assetId":assetId as AnyObject,"userId":gUser_ID as AnyObject]]
        var rating:Float = 0.0
     //   let url = "http://34.200.110.244:9000/getAssetRating"
     /*   ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters){
            (responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict
                let arr = JSON as! NSArray
                let dict = arr.firstObject as! NSDictionary
                if (dict["averagerating"] is NSNull)
                {
                    rating = Float(0.0)
                }
                if (dict["averagerating"] is Int)
                {
                    rating = Float(dict["averagerating"] as! Float)
                }
                self.cosmosview.rating = Double(rating)
                 self.ratingLbl.text = "(\(rating)/\(self.cosmosview.settings.totalStars))"
            }
            else
            {
                self.cosmosview.rating = Double(rating)
                 self.ratingLbl.text = "(\(rating)/\(self.cosmosview.settings.totalStars))"
                print(error?.localizedDescription)
            }
        }*/
        let url = kgetratingUrl + "appname=\(kAppName)&token=\(self.UserId!)&assetId=\(assetId)"
        ApiManager.sharedManager.postDataWithJsonLambda(url: url, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                if JSON["statusCode"] != nil
                {
                    let status = JSON["statusCode"] as! Int
                    if status == 200
                    {
                        let result = JSON["result"] as! NSArray
                        let dict = result.firstObject as! NSDictionary
                        if (dict["averagerating"] is NSNull)
                        {
                            rating = 0.0
                        }
                        if (dict["averagerating"] is Int)
                        {
                            rating = Float(dict["averagerating"] as! Float)
                        }
                        
                        self.cosmosview.rating = Double(rating)
                        self.ratingLbl.text = "(\(rating)/\(self.cosmosview.settings.totalStars))"
                        
                    }
                    else
                    {
                        self.cosmosview.rating = Double(rating)
                        self.ratingLbl.text = "\(rating) / \(self.cosmosview.settings.totalStars)"
                    }
                }
                else
                {
                    self.cosmosview.rating = Double(rating)
                    self.ratingLbl.text = "\(rating) / \(self.cosmosview.settings.totalStars)"
                    
                }
            }
            else
            {
                print(error?.localizedDescription ?? "getrating error")
                self.cosmosview.rating = Double(rating)
                self.ratingLbl.text = "\(rating) / \(self.cosmosview.settings.totalStars)"
            }
        }
    }
    
    // Service call of get assetdata
    @objc func getAssetData(withUrl:String,id:String,userid:String/*,tvshow:Bool*/)
    {
      //  var parameters =  [String:[String:AnyObject]]()
      //  parameters = ["getAssetData":["videoId":id as AnyObject,"userId":userid as AnyObject]]
        let withUrl = kAssestDataUrl + "appname=\(kAppName)&assetId=\(id)&token=\(userid)"
    
        ApiManager.sharedManager.postDataWithJsonLambda(url: withUrl, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                let DetailPage = self.storyboard?.instantiateViewController(withIdentifier: "DetailPage") as! DetailPageViewController
                DetailPage.delegate = self.categoryDelegate
                DetailPage.userid = self.UserId
                DetailPage.deviceId = self.deviceId
                DetailPage.uuid = self.uuid
                DetailPage.storeProdData = self.storeProdData
                DetailPage.Donatedict = self.Donatedict
                let JSON = responseDict
                if JSON is NSArray
                {
                    let dict = JSON as! NSArray
                    DetailPage.TvshowPath = dict.firstObject as! NSDictionary
                }
                else
                {
                    let dict = responseDict as! NSDictionary
                    if dict["statusCode"] != nil
                    {
                        let status = dict["statusCode"] as! Int
                        if status == 200
                        {
                            let result = dict["result"] as! NSDictionary
                            DetailPage.TvshowPath = result
                            self.navigationController?.pushViewController(DetailPage, animated: true)
                        }
                        else
                        {
                            print("json error")
                        }
                    }
                    else
                    {
                        print("json error")
                    }
                }
            }
            else
            {
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                    let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
            self.listcollectionview.isUserInteractionEnabled = true
            self.activityView.removeFromSuperview()
        }
     /*   ApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                
                let DetailPage = self.storyboard?.instantiateViewController(withIdentifier: "DetailPage") as! DetailPageViewController
                let JSON = responseDict
                if JSON is NSArray
                {
                    let dict = JSON as! NSArray
                    DetailPage.TvshowPath = dict.firstObject as! NSDictionary
                }
                else
                {
                    DetailPage.TvshowPath = JSON as! NSDictionary
                }
                DetailPage.delegate = self.categoryDelegate
                DetailPage.userid = self.UserId
                DetailPage.deviceId = self.deviceId
                DetailPage.uuid = self.uuid
                DetailPage.storeProdData = self.storeProdData
                DetailPage.Donatedict = self.Donatedict
                self.navigationController?.pushViewController(DetailPage, animated: true)
                //   self.present(DetailPage, animated: true, completion: nil)
            }
            else
            {
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                    let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }*/
    }
    
    @objc func getaccountInfo(id:String,userid:String/*,tvshow:Bool*/)
    {
      //  var parameters =  [String:[String:AnyObject]]()
      //  parameters = ["getAccountInfo":["deviceId":deviceId as AnyObject,"uuid":uuid as AnyObject]]
      //  let url = kAccountInfoUrl + "appname=\(kAppName)&token=\(userid)"
      /*  let url = "http://34.200.110.244:9000/getAccountInfo"
        ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters){
            (responseDict,error,isDone) in
            if error == nil
            {
                let Json = responseDict
                let dict = Json as! NSDictionary
                accountresponse = (dict.value(forKey: "uuid_exist") as! Bool)
                if accountresponse
                {
                    self.getAssetData(withUrl:kAssestDataUrl,id:id,userid: userid/*,tvshow:tvshow*/)
                }
                else
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.gotoCode()
                }
            }
        }*/
        let url = kAccountInfoUrl + "appname=\(kAppName)&token=\(self.UserId!)"
        ApiManager.sharedManager.postDataWithJsonLambda(url: url, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                let json = responseDict as! NSDictionary
                if json["statusCode"] != nil
                {
                    let status = json["statusCode"] as! Int
                    if status == 200
                    {
                        let result = json["result"] as! NSDictionary
                        accountresponse = result["uuid_exist"] as! Bool
                        if accountresponse == true
                        {
                            self.getAssetData(withUrl:kAssestDataUrl,id:id,userid: userid/*,tvshow:tvshow*/)
                        }
                        else
                        {
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.gotoCode()
                        }
                    }
                    else
                    {
                        print("json error")
                    }
                }
                else
                {
                    print("json error")
                }
                
            }
            else
            {
                print(error?.localizedDescription ?? "getaccountInfo got with error")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @objc func myListdata(mylistDict: NSDictionary)
    {
    }
    @objc func removeListdata(id : String)
    {
        
    }
    @objc func recentlyWatcheddata(recentlyWatchedDict: NSDictionary) {
        
    }
}

extension CategoriesViewController
{
    @objc internal func categoriesSelected(carousalName: String, carousalDetailDict: [[String : Any]]) {
        
        self.menuCollectionList.removeAllObjects()
        let carousal = carousalName
        
        for dict in carousalDetailDict
        {
            if carousal == "Photos"
            {
                self.menuCollectionList.add(dict)
            }
            else if carousal == "Store"
            {
                self.menuCollectionList.add(dict)
            }
            else
            {
                if dict[kCarouselId] is String
                {
                    if (dict[kCarouselId] as! String) == carousalName
                    {
                        self.menuCollectionList.add(dict)
                    }
                }
                if dict[kCarouselId] is NSArray
                {
                    if (((dict[kCarouselId] as! NSArray).firstObject)as! String) == carousalName
                    {
                        self.menuCollectionList.add(dict)
                    }
                }
            }
        }
        self.listcollectionview.isHidden = false
        self.listcollectionview.reloadData()
    }
}

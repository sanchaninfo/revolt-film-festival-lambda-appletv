//
//  ASEpisodesViewController.swift
//  AEOM
//
//  Created by Sanchan on 20/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit
protocol epidelegate:class {
    func getassetdata(withUrl:String,id:String,userid:String)
}


class EpisodesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @objc var contains = NSDictionary()
    @objc var containsDict = NSArray()
    @objc var episodesList = [[String:Any]]()
    @objc var rightHandFocusGuide = UIFocusGuide()
    @objc var UserID,videoId:String!
    @objc var activityView = UIView()
   // var insetButton : UIButton = UIButton()
    var episodeDelegate:myListdelegate!
    @objc var deviceId,uuid: String!
    @objc var storeProdData = [[String:Any]]()
    @objc var Donatedict = [[String:Any]]()
    @objc var isfromsearch = Bool()
    @objc var ispaired = Bool()
    
    @objc var episodeactivityView = UIView()
      var EDelegate:epidelegate?
    
    @IBOutlet weak var insertButton: UIButton!
    @IBOutlet weak var seasontableView: UITableView!
    @IBOutlet weak var eposidetableeView: UITableView!
    @IBOutlet weak var rightHandView: UIView!
    
    @objc var viewToFocus: UIView? = nil
    {
        didSet
        {
            if viewToFocus != nil
            {
                self.setNeedsFocusUpdate();
                self.updateFocusIfNeeded();
            }
        }
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        activityView = ActivityView.init(frame:(self.view.frame))
        self.view.addSubview(activityView)
       
//        insetButton.frame = CGRect(x:630, y: 115, width: 10, height: 10)
//        insetButton.backgroundColor = UIColor.clear
//        insetButton.layer.borderWidth = 1.0
//        insetButton.layer.borderColor = UIColor.black.cgColor
//        insetButton.setTitleColor(UIColor.black, for: .normal)
//        view.addSubview(insetButton)
      
      
     
        self.ispaired = getaccountDetails()
        view.addLayoutGuide(rightHandFocusGuide)
        
        rightHandFocusGuide.bottomAnchor.constraint(equalTo: seasontableView.bottomAnchor).isActive = true
        rightHandFocusGuide.leftAnchor.constraint(equalTo: seasontableView.rightAnchor).isActive = true
        rightHandFocusGuide.topAnchor.constraint(equalTo: seasontableView.topAnchor).isActive = true
        rightHandFocusGuide.rightAnchor.constraint(equalTo: eposidetableeView.leftAnchor).isActive = true
        rightHandFocusGuide.preferredFocusedView = insertButton
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleMenuPress))
        tapGesture.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
        self.view.addGestureRecognizer(tapGesture)

    }
    
    // Number of Section
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    // Number of Rows in a section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        var rowCount:Int = 0
        if tableView == self.seasontableView
        {
            if containsDict.count != 0
            {
               rowCount = 1
            }
            if contains.allKeys.count != 0
            {
                rowCount = contains.allKeys.count
            }
        }
        if tableView == self.eposidetableeView
        {
            if containsDict.count != 0
            {
                rowCount = episodesList.count
            }
            if contains.allKeys.count != 0
            {
                rowCount = episodesList.count
            }
            
        }
        return rowCount
    }
    
    // Cell Item at IndexPath
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = UITableViewCell()
        
        if tableView == self.seasontableView
        {
        
            var tmpSeasonName = "Season"
            var tmpEpisodeName = "Episode"
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            
            cell.textLabel?.font = UIFont.init(name: "Helvetica-Bold", size: 30)
            cell.textLabel?.textColor = UIColor.white
            cell.layer.cornerRadius = 7.0
            cell.selectionStyle = .none
            if contains.allKeys.count != 0
            {
              episodesList = contains[contains.allKeys[indexPath.row]] as! [[String : Any]]
            }
            if containsDict.count != 0
            {
                episodesList = containsDict as! [[String : Any]]
             
            }
            if (episodesList.first)?["seasonCategoryName"] != nil
            {
                if ((episodesList.first)?["seasonCategoryName"] as! String) != ""
                {
                    tmpSeasonName = ((episodesList.first)?["seasonCategoryName"] as! String)
                }
            }
            if (episodesList.first)?["episodeName"] != nil
            {
                if (episodesList.first)?["episodeName"] as! String != ""
                {
                    tmpEpisodeName = ((episodesList.first)?["episodeName"] as! String)
                }
            }
            if containsDict.count != 0
            {
                print(containsDict)
                cell.textLabel?.text = (tmpSeasonName + "  " + "\((((containsDict.firstObject as! NSDictionary)["seasonCategoryNo"]) as! String))" + "       " + " " + tmpEpisodeName + "   " + "\(containsDict.count)")
            }
            if contains.allKeys.count != 0
            {
                  cell.textLabel?.text = (tmpSeasonName + "  " + "\(contains.allKeys[indexPath.row])s" + "       " + " " + tmpEpisodeName + "   " + "\((contains[contains.allKeys[indexPath.row]] as! NSArray).count)")
            }
         
        
            
        }
        if tableView == self.eposidetableeView
        {
        
            var episoddss = [[String:Any]]()
      
            if contains.allKeys.count != 0
            {
               episoddss = episodesList.sorted(by: {Int($0["episodeNo"] as! String)! < Int($1["episodeNo"] as! String)!})
            }
            if containsDict.count != 0
            {
                episoddss = episodesList
               // episoddss = episodesList.sorted(by: {Int(($0["metadata"] as! NSDictionary)["episode_number"] as! String)! < Int(($1["metadata"] as! NSDictionary)["episode_number"] as! String)!})
            }
          //  episoddss = episodesList.sorted(by: {Int($0["episode_number"] as! String)! < Int($1["episode_number"] as! String)!})
       
            
            let path = episoddss[indexPath.row]
            var tmpSeasonName = "Season"
            var tmpEpisodeName = "Episode"
            if path["seasonCategoryName"] != nil
            {
                if (path["seasonCategoryName"] as! String) != ""
                {
                    tmpSeasonName = (path["seasonCategoryName"] as! String)
                }
            }
            if path["episodeName"] != nil
            {
                if (path["episodeName"] as! String) != ""
                {
                    tmpEpisodeName = (path["episodeName"] as! String)
                }
            }
           
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath)
            let img = (cell.viewWithTag(25) as! UIImageView)
            img.kf.indicatorType = .activity
           // (cell.viewWithTag(25) as! UIImageView).kf.setImage(with: URL(string: path["thumb"] as! String))
            if path["metadata"] != nil
            {
                if (isnil(json: (path["metadata"] as! NSDictionary), key: "movie_art")) == ""
                {
                    (cell.viewWithTag(25) as! UIImageView).image = UIImage(imageLiteralResourceName: "RevoltFlat")
                }
                else
                {
                    (cell.viewWithTag(25) as! UIImageView).kf.setImage(with: URL(string:isnil(json: (path["metadata"] as! NSDictionary), key: "movie_art")))
                }
                (cell.viewWithTag(24) as! UILabel).text = "\(tmpSeasonName)\((path["metadata"] as! NSDictionary)["season_number"] as! String): \(tmpEpisodeName)\((path["metadata"] as! NSDictionary) ["episode_number"] as! String)"
            }
            else
            {
                if (isnil(json: (path as NSDictionary), key: "landscape_url_500x281")) == ""
                {
                    (cell.viewWithTag(25) as! UIImageView).image = UIImage(imageLiteralResourceName: "RevoltFlat")
                }
                else
                {
                    (cell.viewWithTag(25) as! UIImageView).kf.setImage(with: URL(string:isnil(json: (path as NSDictionary), key: "landscape_url_500x281")))
                }
              
                 (cell.viewWithTag(24) as! UILabel).text = "\(tmpSeasonName)\((path)["seasonCategoryNo"] as! String): \(tmpEpisodeName)\((path) ["episodeNo"] as! String)"
            }
          
         //
            (cell.viewWithTag(27) as! UIProgressView).isHidden = true
            let DescriptionText = path["description"] as! String
            let destxt = DescriptionText.replacingOccurrences(of: "&", with: "", options: .literal, range: nil)
            let destxt1 =  destxt.replacingOccurrences(of: "amp", with: "", options: .literal, range: nil)
            let destxt2 = destxt1.replacingOccurrences(of: ";", with: "", options: .literal, range: nil)
            (cell.viewWithTag(26) as! UITextView).text = destxt2
        }
        return cell
    }
    
    // Did Update focus
    func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        
        if tableView == self.seasontableView
        {
            guard let nextView = context.nextFocusedView else { return }
            if (nextView == insertButton)
            {
                guard let indexPath = context.previouslyFocusedIndexPath, let cell = tableView.cellForRow(at: indexPath) else
                {
                    return
                }
             //   insetButton.isEnabled = false
                cell.textLabel?.textColor = UIColor.white
                rightHandFocusGuide.preferredFocusedView = cell
              //  viewToFocus = self.eposidetableeView
                
            }
            else
            {
                rightHandFocusGuide.preferredFocusedView = insertButton
            }
            
            guard let indexPath = context.nextFocusedIndexPath, let cell = tableView.cellForRow(at: indexPath)
                else { return }
            cell.textLabel?.textColor = UIColor.black
            if containsDict.count != 0
            {
                episodesList = containsDict as! [[String : Any]]
            }
            if contains.allKeys.count != 0
            {
               episodesList = contains[contains.allKeys[indexPath.row]] as! [[String : Any]]
            }
            
            eposidetableeView.reloadData()
            
            guard let prevIndexPath = context.previouslyFocusedIndexPath, let prevCell = tableView.cellForRow(at: prevIndexPath)
                else { return }
            prevCell.textLabel?.textColor = UIColor.white
        }
        if tableView == self.eposidetableeView
        {
            if let previousIndexPath = context.previouslyFocusedIndexPath,
                let cell = tableView.cellForRow(at: previousIndexPath)
            {
                (cell.viewWithTag(25) as! UIImageView).transform = .identity
                cell.focusStyle = .custom
                cell.selectionStyle = .none
            }
            if let indexPath = context.nextFocusedIndexPath,
                let cell = tableView.cellForRow(at: indexPath)
            {
                (cell.viewWithTag(25) as! UIImageView).adjustsImageWhenAncestorFocused = true
                cell.focusStyle = .custom
                cell.selectionStyle = .none
            }
        }
    }
    
    // Select item at IndexPath
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       var path = NSDictionary()
        if tableView == self.eposidetableeView
        {
            if containsDict.count != 0
            {
                path = containsDict[indexPath.row] as! NSDictionary
            }
            else
            {
                path = episodesList[indexPath.row] as NSDictionary
            }
            activityView = ActivityView.init(frame:(self.view.frame))
            self.view.addSubview(activityView)
            self.eposidetableeView.isUserInteractionEnabled = false
            if ispaired
            {
                RecentlyWatched(withurl: kRecentlyWatchedUrl, videoId: path["assetId"] as! String, userId:self.UserID, row:indexPath)
            }
            else
            {
                pairingPage()
            }
         //   RecentlyWatched(withurl: kRecentlyWatchedUrl, videoId: path["assetId"] as! String, userId:self.UserID, row:indexPath)
            
        }
    }
    
    
    @objc func pairingPage()
    {
        var parameters = [String:[String:AnyObject]]()
        parameters = ["getActivationCode":["model":"AppleTV4Gen" as AnyObject,"manufacturer":"Apple" as AnyObject,"device_name":"AppleTV" as AnyObject, "device_id":deviceId! as AnyObject,"device_mac_address":"mac" as AnyObject,"brand_name":"Apple" as AnyObject,"host_name":"app" as AnyObject,"display_name":"apple" as AnyObject, "serial_number":"1234" as AnyObject,"version":"DameDashStudios" as AnyObject,"platform":"TVOS" as AnyObject]]
        ApiManager.sharedManager.postDataWithJson(url: kActivationCodeUrl, parameters: parameters) { (responseDict, error,isDone)in
            if error == nil
            {
                let dict = responseDict as! [String:String]
                self.uuid = dict["uuid"]
                self.deviceId = dict["device_id"]
                
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                //                                    if !((dict["pair_devce"]! as String).contains("active"))
                //                                    {
                //    let rootView = storyBoard.instantiateViewController(withIdentifier: "Code") as! CodeViewController
                let rootView = storyBoard.instantiateViewController(withIdentifier: "PPCode") as! PPCodeViewController
                rootView.uuid = self.uuid
                rootView.deviceId = self.deviceId
                rootView.code = dict["code"]
                if self.isfromsearch
                {
                    self.present(rootView, animated: false, completion: nil)
                }
                else
                {
                    self.navigationController?.pushViewController(rootView, animated: true)
                }
                // }
            }
        }
    }
    @objc func getaccountDetails() -> Bool
    {
     //   var parameters =  [String:[String:AnyObject]]()
     //   parameters = ["getAccountInfo":["deviceId":deviceId as AnyObject,"uuid":uuid as AnyObject]]
      /*  let url = kAccountInfoUrl + "appname=\(kAppName)&device_id=\(deviceId!)&uuid=\(uuid!)"
        var accountstat = Bool()
        ApiManager.sharedManager.postDataWithJsonLambda(url: url, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                let json = responseDict
          
            }
            else
            {
                print(error?.localizedDescription)
            }
        }*/
       // let url = "http://34.200.110.244:9000/getAccountInfo"
        var accountstat = Bool()
//        ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters){
//            (responseDict,error,isDone) in
//            if error == nil
//            {
//                let Json = responseDict
//                let dict = Json as! NSDictionary
//                self.ispaired = (dict.value(forKey: "uuid_exist") as! Bool)
//                accountstat = (dict.value(forKey: "uuid_exist") as! Bool)
//                if accountstat == true
//                {
//                   // self.UserID = (dict.value(forKey: "_id") as! String)
//                }
//
//            }
//            else
//            {
//                print(error?.localizedDescription)
//
//            }
//        }
        
        let url = kAccountInfoUrl + "appname=\(kAppName)&token=\(self.UserID!)"
        ApiManager.sharedManager.postDataWithJsonLambda(url: url, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                let json = responseDict as! NSDictionary
                if json["statusCode"] != nil
                {
                    let status = json["statusCode"] as! Int
                    if status == 200
                    {
                        let result = json["result"] as! NSDictionary
                        self.ispaired = result["uuid_exist"] as! Bool
                        accountstat = result["uuid_exist"] as! Bool
                    }
                    else
                    {
                        print("json error")
                    }
                }
                else
                {
                    print("json error")
                }
                self.activityView.removeFromSuperview()
                
            }
            else
            {
                print(error?.localizedDescription ?? "getaccountInfo got with error")
            }
        }
        return accountstat
        
    }
    
    // Service call for create recently watched
    @objc func RecentlyWatched(withurl:String,videoId:String,userId:String,row:IndexPath)
    {
        
        let withurl = kRecentlyWatchedUrl + "appname=\(kAppName)&videoId=\(videoId)&token=\(userId)&seekTime=0"
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let DetailPage = storyBoard.instantiateViewController(withIdentifier: "DetailPage") as! DetailPageViewController
        ApiManager.sharedManager.postDataWithJsonLambda(url: withurl, parameters: ["appname":"revoltfilm"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                if JSON["statusCode"] != nil
                {
                    let status = JSON["statusCode"] as! Int
                    if status == 200
                    {
                        kisRecentlyUpdated = "RecentlyUpdated"

                        let result = JSON["result"] as! NSDictionary
                        let dicton = result["watchList"] as! NSDictionary
                        self.getAssetData(withUrl: kAssestDataUrl, id: dicton["videoId"] as! String, userid: self.UserID,row:row)
                        DetailPage.delegate = self.episodeDelegate
                        self.episodeDelegate?.recentlyWatcheddata(recentlyWatchedDict: dicton)
                        
                    }
                    else
                    {
                       print("json error in recently watched call with status code \(status)")
                    }
                }
                else
                    
                {
                    print("json error with out statusCode")
                }
                
            }
            else
            {
                print(error?.localizedDescription ?? "json error in recently watched")
            }
        }
        
        
 /*       let parameters = ["createRecentlyWatched": ["videoId":videoId,"userId":userId]]
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let DetailPage = storyBoard.instantiateViewController(withIdentifier: "DetailPage") as! DetailPageViewController
        ApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters as [String : [String : AnyObject]]){
            (responseDict,error,isDone)in
            if error == nil
            {
                kisRecentlyUpdated = "RecentlyUpdated"
                let JSON = responseDict
                let dict = JSON as! NSDictionary
                let dicton = dict["recentlyWatched"] as! NSDictionary
//                if (dicton["seekTime"] as! Float64) > 0
//                {
//                    DetailPage.isRecentlywatch = true
//                }
                self.getAssetData(withUrl: kAssestDataUrl, id: dicton["videoId"] as! String, userid: self.UserID,row:row)
                DetailPage.delegate = self.episodeDelegate
                self.episodeDelegate?.recentlyWatcheddata(recentlyWatchedDict: dicton)
            }
            else
            {
                print("json error")
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                 let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }*/
    }
    
    @objc func getAssetData(withUrl:String,id:String,userid:String,row:IndexPath)
    {
      //  var parameters =  [String:[String:AnyObject]]()
        var path = NSDictionary()
        _ = eposidetableeView.cellForRow(at: row)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let PlayerVC = storyBoard.instantiateViewController(withIdentifier: "playerLayer") as! PlayerLayerViewController
        var user_ID = String()
        if userid == ""
        {
            user_ID = "guest_id"
        }
        else
        {
            user_ID = userid
        }
     //   parameters = ["getAssetData":["videoId":id as AnyObject,"userId":user_ID as AnyObject]]
        
         let withUrl = kAssestDataUrl + "appname=\(kAppName)&assetId=\(id)&token=\(user_ID)"
        ApiManager.sharedManager.postDataWithJsonLambda(url: withUrl, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict
                
                if JSON is NSArray
                {
                    let dict = JSON as! NSArray
                    path = dict.firstObject as! NSDictionary
                }
                else
                {
                    
                    let dict = responseDict as! NSDictionary
                    if dict["statusCode"] != nil
                    {
                        let status = dict["statusCode"] as! Int
                        if status == 200
                        {
                            let result = dict["result"] as! NSDictionary
                            path = result
                        }
                        else
                        {
                            print("json error")
                        }
                    }
                    else
                    {
                        print("json error")
                    }
                }
                var monotize = String()
                var monotizeLabel = String()
                var montizetype = String()
         //       var montizeURL = String()
                let subscriptionPage = storyBoard.instantiateViewController(withIdentifier: "subscription") as! SubscriptionViewController
                
                if path.object(forKey: "metadata") != nil
                {
                    let metaDict = path.object(forKey: "metadata") as! NSDictionary
                    monotize = metaDict["monetize"] as! String
                    monotizeLabel = metaDict["monetizeLabel"] as! String
                    montizetype = metaDict["monetizeType"] as! String
                }
                else
                {
                    monotize = path["monetize"] as! String
                    monotizeLabel = path["monetizeLabel"] as! String
                    montizetype = path["monetizeType"] as! String
                }
                
                
                //                if path["watchedVideo"] != nil
                //                {
                if path["m3u8_url"] != nil && path["m3u8_url"] as! String != ""
                {
                    PlayerVC.videoUrl = path["m3u8_url"] as! String
                }
                else
                {
                    if path["url"] != nil && path["url"] as! String != ""
                    {
                        PlayerVC.videoUrl = path["url"] as! String
                    }
                    else
                    {
                        if path["mp4_url"] != nil && path["mp4_url"] as! String != ""
                        {
                            PlayerVC.videoUrl = path["mp4_url"] as! String
                        }
                    }
                }
                
                PlayerVC.mainVideoID = path["assetId"] as! String
                PlayerVC.deviceID = self.deviceId
                PlayerVC.uuid = self.uuid
                PlayerVC.DonateData = self.Donatedict
                PlayerVC.storeData = self.storeProdData
                PlayerVC.userID = self.UserID
                PlayerVC.videoID = (path["assetId"] as! String)
                PlayerVC.monitizeLbl = monotizeLabel
                PlayerVC.monitizetype = montizetype
                
                if (Double(((path["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] as! String)! > 0.0)
                {
                    PlayerVC.isResume = true
                    PlayerVC.resumeTime = Float64(((path["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] as! String)!
                    
                    //                            print(path["watchedVideo"])
                    //                            print((Int(path["file_duration"] as! String))! / 60)
                    //                            print(Float((Int(path["file_duration"] as! String))! / 60) / ((path["watchedVideo"] as! Float)))
                    //   (cell?.viewWithTag(27) as! UIProgressView).isHidden = false
                    //   (cell?.viewWithTag(27) as! UIProgressView).progress = (Float((Int(path["file_duration"] as! String))! / 60) / ((path["watchedVideo"] as! Float)))
                    //    print((cell?.viewWithTag(27) as! UIProgressView).progress)
                }
                else
                {
                    PlayerVC.isResume = false
                    
                }
                if monotize == "true"
                {
                    if (path["monetize_data_source_url"] as! String) == "Products.json"
                    {
                        PlayerVC.isDonate = false
                    }
                    else
                    {
                        PlayerVC.isDonate = true
                        
                    }
                }
                else
                {
                    PlayerVC.isDonate = false
                }
                if self.isfromsearch
                {
                    self.present(PlayerVC, animated: false, completion: nil)
                }
                else
                {
                    if (((path["userData"] as! NSArray).firstObject as! NSDictionary)["subscription_status"] as! String) == "active"
                    {
                        self.navigationController?.pushViewController(PlayerVC, animated: true)
                    }
                    else
                    {
                        self.navigationController?.pushViewController(subscriptionPage, animated: true)
                    }
                    // self.navigationController?.pushViewController(PlayerVC, animated: false)
                }
                
                //   self.present(PlayerVC, animated: true, completion: nil)
                //  }
                
            }
            else
            {
                print(error?.localizedDescription ?? "json error in getassetdata")
            }
            self.activityView.removeFromSuperview()
        }
        
     /*   ApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
              
                if JSON is NSArray
                {
                    let dict = JSON as! NSArray
                    path = dict.firstObject as! NSDictionary
                }
                else
                {
                     path = JSON as! NSDictionary
                }
                var monotize = String()
                var monotizeLabel = String()
                var montizetype = String()
                var montizeURL = String()
                let subscriptionPage = storyBoard.instantiateViewController(withIdentifier: "subscription") as! SubscriptionViewController
                
                if path.object(forKey: "metadata") != nil
                {
                    let metaDict = path.object(forKey: "metadata") as! NSDictionary
                    monotize = metaDict["monetize"] as! String
                    monotizeLabel = metaDict["monetizeLabel"] as! String
                    montizetype = metaDict["monetizeType"] as! String
                }
                else
                {
                    monotize = path["monetize"] as! String
                    monotizeLabel = path["monetizeLabel"] as! String
                    montizetype = path["monetizeType"] as! String
                }
                
                
//                if path["watchedVideo"] != nil
//                {
                    if path["m3u8_url"] != nil && path["m3u8_url"] as! String != ""
                    {
                        PlayerVC.videoUrl = path["m3u8_url"] as! String
                    }
                    else
                    {
                        if path["url"] != nil && path["url"] as! String != ""
                        {
                            PlayerVC.videoUrl = path["url"] as! String
                        }
                        else
                        {
                            if path["mp4_url"] != nil && path["mp4_url"] as! String != ""
                            {
                                PlayerVC.videoUrl = path["mp4_url"] as! String
                            }
                        }
                    }
                    
                    PlayerVC.mainVideoID = path["assetId"] as! String
                    PlayerVC.deviceID = self.deviceId
                    PlayerVC.uuid = self.uuid
                    PlayerVC.DonateData = self.Donatedict
                    PlayerVC.storeData = self.storeProdData
                    PlayerVC.userID = self.UserID
                    PlayerVC.videoID = (path["assetId"] as! String)
                    PlayerVC.monitizeLbl = monotizeLabel
                    PlayerVC.monitizetype = montizetype
                    
                    if (((path["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] as! Float64 > 0)
                    {
                        PlayerVC.isResume = true
                        PlayerVC.resumeTime = (((path["userData"] as! NSArray).firstObject as! NSDictionary)["watchedVideo"] as! Float64)
                        
                        //                            print(path["watchedVideo"])
                        //                            print((Int(path["file_duration"] as! String))! / 60)
                        //                            print(Float((Int(path["file_duration"] as! String))! / 60) / ((path["watchedVideo"] as! Float)))
                        //   (cell?.viewWithTag(27) as! UIProgressView).isHidden = false
                        //   (cell?.viewWithTag(27) as! UIProgressView).progress = (Float((Int(path["file_duration"] as! String))! / 60) / ((path["watchedVideo"] as! Float)))
                        //    print((cell?.viewWithTag(27) as! UIProgressView).progress)
                    }
                    else
                    {
                        PlayerVC.isResume = false
                        
                    }
                    if monotize == "true"
                    {
                        if (path["monetize_data_source_url"] as! String) == "Products.json"
                        {
                            PlayerVC.isDonate = false
                        }
                        else
                        {
                            PlayerVC.isDonate = true
                            
                        }
                    }
                    else
                    {
                        PlayerVC.isDonate = false
                    }
                    if self.isfromsearch
                    {
                        self.present(PlayerVC, animated: false, completion: nil)
                    }
                    else
                    {
                        if (((path["userData"] as! NSArray).firstObject as! NSDictionary)["subscription_status"] as! String) == "active"
                        {
                            self.navigationController?.pushViewController(PlayerVC, animated: true)
                        }
                        else
                        {
                            self.navigationController?.pushViewController(subscriptionPage, animated: true)
                        }
                      // self.navigationController?.pushViewController(PlayerVC, animated: false)
                    }
                   
                 //   self.present(PlayerVC, animated: true, completion: nil)
             //  }
                
               
                
            }
            else
            {
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                let _ = self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }*/
    }
    @objc func handleMenuPress()
    {
      //  self.EDelegate?.getassetdata(withUrl: kAssestDataUrl, id: videoId,userid: UserID)
        if !isfromsearch
        {
            
            for viewcontroller in (self.navigationController?.viewControllers)!
            {
                if viewcontroller.isKind(of: DetailPageViewController.self)
                {
                    let  _ =  self.navigationController?.popToViewController(viewcontroller, animated: false)
                }
            }
        }
        else
        {
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    @objc func myListdata(mylistDict: NSDictionary)
    {
    }
    @objc func removeListdata(id : String)
    {
    }
    @objc func recentlyWatcheddata(recentlyWatchedDict: NSDictionary)
    {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//
//  ProviderViewController.swift
//  Revolt
//
//  Created by Sanchan Mac Mini 2 on 19/09/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit


protocol  provider:class {
    func getassetdata(withUrl:String,id:String,userid:String)
}
class ProviderViewController: UIViewController {

    @IBOutlet weak var providerLbl: UILabel!
    @IBOutlet weak var Img: UIImageView!
    @objc var videoId,UserId:String!
    var provideDelegate: provider?

    override func viewDidLoad() {
        super.viewDidLoad()
      
        Img.layer.cornerRadius = Img.frame.height/2
        Img.clipsToBounds = true
        providerLbl.text = "Please verify your Subscription \n To subscribe go to \(kBaseUrl)"

        // Do any additional setup after loading the view.
    }

    @IBAction func OKBtn(_ sender: Any) {
       // self.provideDelegate?.getassetdata(withUrl:kAssestDataUrl, id: self.videoId, userid: self.UserId)
     //   self.navigationController?.popViewController(animated: true)
//        for viewcontroller in (self.navigationController?.viewControllers)!
//        {
//            if viewcontroller.isKind(of: MainViewController.self)
//            {
//                let  _ =  self.navigationController?.popToViewController(viewcontroller, animated: false)
//            }
//        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.gotoCode()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
